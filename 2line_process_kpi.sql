--Сессия создания таски
select sssn.id as session_create_id, mastersber."user".login as first_user, sssn.created_at as session_create_start, sssn.closed_at as session_create_end, tsk.id as tsk_id,
  tsk.created_at as task_created from mastersber.crm_task_session  as sssn
  left join mastersber.crm_task tsk on tsk.deal_id = sssn.deal_id
  left join mastersber."user" on sssn.cas_user_id = mastersber."user".cas_id
  where tsk.created_at >= sssn.created_at and tsk.created_at<=sssn.closed_at
---Сессия изменения таски
select sssn.id as session_update_id, mastersber."user".login as last_update_user, sssn.created_at as session_start, sssn.closed_at as session_end,tsk.id as tsk_id,
  tsk.updated_at as task_created, tsk.status_id from mastersber.crm_task_session  as sssn
  left join mastersber.crm_task tsk on tsk.deal_id = sssn.deal_id
  left join mastersber."user" on sssn.cas_user_id = mastersber."user".cas_id
  where tsk.updated_at >= sssn.created_at and tsk.updated_at<=sssn.closed_at

---Выполненные таски c логином исполнителя
select sssn.id as session_update_id, sssn.deal_id,mastersber."user".login as last_update_user, mastersber."user".second_name||' '||mastersber."user".first_name as fio_mik,
  sssn.created_at as session_start, sssn.closed_at as session_end,tsk.id as tsk_id,
  tsk.task_type_id,   tsk.updated_at as task_last_updated,EXTRACT(WEEK FROM tsk.updated_at) as week_num, tsk.status_id, tsk_hstr.created_at,
  tsk_price.task_price, crm_tsk_type.name, ms_deal.person.id, prsn.last_name||' '||prsn.first_name ||' '||prsn.middle_name as client_fio
from mastersber.crm_task as tsk
  left join mastersber.crm_task_session  sssn  on tsk.deal_id = sssn.deal_id
  left join mastersber.crm_task_history tsk_hstr on tsk.id = tsk_hstr.task_id
  left join mastersber."user"  on tsk_hstr.cas_user_id = mastersber."user".cas_id
  left join sbrf."2line_task_price" tsk_price on tsk_price.task_type_id = tsk.task_type_id
  left join mastersber.crm_task_type crm_tsk_type on tsk.task_type_id = crm_tsk_type.id
  left join ms_deal.person on ms_deal.person.deal_id = tsk.deal_id
  left join ms_person.person prsn on prsn.id = ms_deal.person.person_id
where tsk_hstr.created_at >= sssn.created_at and tsk_hstr.created_at<=sssn.closed_at
      and tsk_hstr.old_status_id <>2 and tsk_hstr.new_status_id = 2
      and tsk.status_id = 2 and tsk.active is true
      and tsk.created_at >= '2018-07-02'
      and ms_deal.person.person_type_id = 21030


---Табель
select *
from sbrf.t_dwh_ip_mik_tab
where date>='2018-07-02' and date <='2018-07-16'
and user_id = 189762
order by date desc

select type from sbrf.t_dwh_ip_mik_tab
group by type

--Таски с номерами сессии создания/изменения
select tsk.id, tsk.task_type_id, tsk.status_id, session_create.session_create_id , session_update.session_update_id from mastersber.crm_task as tsk
left join (select sssn.id as session_create_id, sssn.created_at as session_create_start, sssn.closed_at as session_create_end, tsk.id as tsk_id,
             tsk.created_at as task_created from mastersber.crm_task_session  as sssn
  left join mastersber.crm_task tsk on tsk.deal_id = sssn.deal_id
  where (tsk.created_at) >= sssn.created_at and (tsk.created_at) <=sssn.closed_at) session_create
on session_create.tsk_id = tsk.id
left join (select sssn.id as session_update_id, sssn.cas_user_id as task_update_author, sssn.created_at as session_start,
             sssn.closed_at as session_end,tsk.id as tsk_id,  tsk.updated_at as task_created, tsk.status_id from mastersber.crm_task_session  as sssn
  left join mastersber.crm_task tsk on tsk.deal_id = sssn.deal_id
  where (tsk.updated_at + interval '5 hour')>= sssn.created_at and tsk.updated_at<=sssn.closed_at) session_update
on session_update.tsk_id = tsk.id
where tsk.task_type_id >=10 and tsk.active is true and  tsk.created_at >= '2018-07-02'

--Запрос для выборки задач на увеличение суммы после которых сумма была увеличена до нужной клиенту
select mastersber.crm_task.id, (ms_deal.deal_data.requested_sum <= ms_deal.deal_data.approved_sum) as task_done from mastersber.crm_task
left join ms_deal.deal_data on mastersber.crm_task.deal_id = ms_deal.deal_data.deal_id
where task_type_id = 21 and active is true and status_id =2 and approved_sum is not null
-------------------------------------------------------------------------------------

---Запрос для учета внесения информации о риелторе (таска 50)
select tsk.id, tsk.crm_deal_id, prsn.person_id, prsn.modified_time, prsn.person_type_id  from (select  updated_at, id, crm_deal_id from mastersber.crm_task
where active is true and task_type_id = 50 and status_id = 2) as tsk
left join ms_deal.person prsn on tsk.crm_deal_id = prsn.deal_id

where (person_type_id in (21070,21080,21080,21090,21060) or person_type_id is null)


select * from ms_deal.dictionary where type = 'dict_person_type'



select crm_task_history.* from mastersber.crm_task_history
  left join mastersber.crm_task on task_id = crm_task.id
where mastersber.crm_task_history.old_status_id = 1 and mastersber.crm_task_history.new_status_id = 2
and mastersber.crm_task.task_type_id not in (1,2,3)
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 22 16:04:03 2017

@author: azazello
."""

from PyQt5.QtCore import QUrl, QDir, Qt, pyqtSignal, QObject,QMimeData,QByteArray,QDataStream,QIODevice, QPoint, QMetaObject,QCoreApplication,QTranslator 
from PyQt5.QtGui import QImage, QPainter, QPalette, QPixmap ,QMouseEvent, QTransform,QIcon,QFont,QDrag,QDropEvent,QInputEvent
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QLabel,QGridLayout,QVBoxLayout,QComboBox,
        QMainWindow, QMenu, QMessageBox, QScrollArea, QSizePolicy, QPushButton,QWidget, QHBoxLayout,QFrame,QSplitter,QTextEdit, QGroupBox, QSpacerItem, QWidgetItem )
from PyQt5.QtPrintSupport import QPrintDialog, QPrinter
from math import ceil
from DocFile import DocFile
import pandas as pd
import os
import myPDF

doc_ext_list = ['.docx','.doc','.rtf','.odt']
img_ext_list = ['.jpg','.jpeg','.bmp','.tiff','.tif','.png']


class Communicate(QObject):
    printText = pyqtSignal(str)

    def write(self, text):
        self.printText.emit(text)

    def flush(self):
        pass



        
class clickableLabel(QLabel):
    clicked = pyqtSignal()
    path = str()
    selected = False
    rotated = 0
    group = str()
    filetype = str()
    filename = str()
    row  = None
    user_check_needed = False
    _heightForWidthFactor = float
    def __init__(self, text, row):
        super(QLabel, self).__init__(text)
        self.row = row
        self.path = row['path']
        self.group = row['doc_type']
        self.filename = os.path.basename(self.path)
        self.filetype = os.path.splitext(self.path)[1]
        self.user_check_needed = row['user_check_needed']
        self.setAttribute(Qt.WA_DeleteOnClose)

        if (self.user_check_needed):
               self.setStyleSheet( "QLabel {"
                             "border-style: solid;"
                             "border-width: 1px;"
                             "border-color: orange; "
                             "}")  
        self.clicked.connect(self.select_image_change)
        self.setAcceptDrops(True)
        self.setAlignment(Qt.AlignLeft)
    def contextMenuEvent(self, event):
            menu = self.window().editMenu
            self.select_image()
            menu.exec_(self.mapToGlobal(event.pos()))
    def unselect_image(self):
        main_window = self.window()
        main_window.del_select_image(self)
        self.selected =False
        self.setStyleSheet( "QLabel {"
                      "border-style: solid;"
                      "border-width: 0px;"
                      "border-color: black; "
                      "}")
        main_window.editMenu.menuAction().setEnabled(len(main_window.selected_images)>0)             
    
       
    def select_image(self):
       self.selected =True
       self.setStyleSheet( "QLabel {"
                             "border-style: solid;"
                             "border-width: 2px;"
                             "border-color: green; "
                             "}")
       main_window = self.window()
       main_window.add_select_image(self)
       main_window.editMenu.menuAction().setEnabled(True) 
       
    def select_image_change(self):
        if (self.selected):
               self.unselect_image()
        else:
               self.select_image()
    

       
    def mouseReleaseEvent(self, e):
        QLabel.mouseReleaseEvent(self, e)
        #if e.buttons() == Qt.LeftButton:        
        if (QCoreApplication.instance().keyboardModifiers() == Qt.ControlModifier):
               self.clicked.emit()
        elif (QCoreApplication.instance().keyboardModifiers() == Qt.ShiftModifier):
               main_window = self.window()
               if (len(main_window.selected_images)>0):
                      widget1 = main_window.selected_images[len(main_window.selected_images)-1]
                      
                      widget2 = self
                      main_window.select_from_widget_to_widget(widget1,widget2)
                      main_window.updateActions()
               else:
                      self.clicked.emit()
        elif (e.button() == Qt.LeftButton):
               
               main_window = self.window()
               selected = self.selected
               main_window.unselect_images()
               self.selected = selected
               self.clicked.emit()
    def mouseMoveEvent(self, e):

        if e.buttons() != Qt.LeftButton:
            return

        mimeData = QMimeData()

        drag = QDrag(self)
        drag.setMimeData(mimeData)
        drag.setHotSpot(e.pos() - self.rect().topLeft())

        dropAction = drag.exec_(Qt.MoveAction)
        


    def mousePressEvent(self, e):

        QLabel.mousePressEvent(self, e)

        if e.button() == Qt.RightButton:
            return
        child = self
        if (child==None):
               return
        pixmap = child.pixmap()
        itemData= QByteArray()
        #dataStream= QDataStream(itemData, QIODevice.WriteOnly)
        #pixmap = QPoint(e.pos() - child.pos())
        #dataStream.pixmap
        mimeData = QMimeData()
        mimeData.setData("application/x-dnditemdata", itemData)
        drag = QDrag(self)
        drag.setMimeData(mimeData)
        drag.setPixmap(pixmap)
        drag.setHotSpot(e.pos() - child.pos());
            
    def mouseDoubleClickEvent(self, e):
        os.startfile(self.path)
        #self.clicked.emit()

    def dragEnterEvent(self, e):
           e.accept()

    def dropEvent(self, e:QDropEvent):

        #position = e.pos()
        widget:clickableLabel = e.source()
        mimeData:QMimeData = e.mimeData()
        if (mimeData.hasUrls()):
               urlList:list = mimeData.urls()
               i=0
               for url in urlList:
                     print(url.toLocalFile())
                     i=i+1
                     
        if (self != widget):
               main_window = widget.window()
               oldGroup:docQGroupBox = main_window.searchdocGroup(widget.group)
               #oldGroup.removeWidget(widget, True)
               newGroup:docQGroupBox = main_window.searchdocGroup(self.group)
               print( widget.path+' -> '+self.path)
               oldwidget:clickableLabel = self
               widget.group = self.group
               if (oldGroup==newGroup):
                      newPosition = newGroup.docWidgets.index(widget)
                      selfPosition = oldGroup.docWidgets.index(self)
                      if ( (selfPosition-newPosition)==1 ):
                             if (len(oldGroup.docWidgets)>(selfPosition+1)):
                                    oldwidget = oldGroup.docWidgets[selfPosition+1]
                             else:
                                    oldGroup.removeWidget(widget, True)
                                    newGroup.addWidget(widget)
                                    e.accept()
                                    return
               oldGroup.removeWidget(widget, True)
                             
               newGroup.addWidgetBeforeWidget(oldwidget, widget)
              
               e.accept()
        else:
               e.ignore()
                

        
    def rotate(self, degree):
        pixmap = self.pixmap()
        pixmap = pixmap.transformed(QTransform().rotate(degree))
        self.setPixmap(pixmap)
        self._heightForWidthFactor = 1.0*float(pixmap.size().width())/float(pixmap.size().height())
        if (degree==90 or degree == 270):
               self.setFixedWidth(pixmap.size().width())
        
        self.rotated = self.rotated + degree
        if (self.rotated>360):
               self.rotated =self.rotated-360
        
              
              
    def setPixmap(self,pixmap):
        painter = QPainter(pixmap)
        painter.setPen(Qt.red);
        painter.setFont(QFont("Arial",28))
        painter.drawText(pixmap.rect(),Qt.TextWordWrap,self.filename)
        QLabel.setPixmap(self, pixmap)
        image = pixmap
        self._heightForWidthFactor = 1.0*float(image.size().width())/float(image.size().height())
        
    def hasWidthForHeight(self):
        # This tells the layout manager that the banner's height does depend on its width
        return True   
    
    def hasHeightForWidth(self):
        # This tells the layout manager that the banner's height does depend on its width
        return False
 
    def widthForHeight(self, height):
        # This tells the layout manager what the preferred and minimum height are, for a given width
        return ceil(height * self._heightForWidthFactor)

    def heightForWidth(self, width):
        # This tells the layout manager what the preferred and minimum height are, for a given width
        return ceil(width * self._heightForWidthFactor)
    
    def _resizeImage(self, size):
        # Since we're keeping _heightForWidthFactor, we can code a more efficient implementation of this, too
        height = size.height()
        width = self.heightForWidth(height)
        self.setFixedSize(width, height)
   
    def resizeEvent(self, event):
        super(QLabel, self).resizeEvent(event)
        # For efficiency, we pass the size from the event to _resizeImage()
        self._resizeImage(event.size())



class docQGroupBox(QGroupBox):
       scrollLayout= QHBoxLayout()
       docWidgets= list()
       name=str()
       df = pd.DataFrame()
       def __init__(self, text, df=None):
              super(QGroupBox, self).__init__(text)

              self.docWidgets= list()
              self.name = text
              listBox = QGridLayout(self)
              self.setLayout(listBox)
              self.scrollArea = QScrollArea(self)
              #self.scrollArea.setFixedHeight(300)
              listBox.addWidget(self.scrollArea)
              
              #self.scrollArea.setMinimumHeight(self.scrollLayout.sizeHint().height() + self.scrollArea.horizontalScrollBar().sizeHint().height())
              self.scrollArea.setWidgetResizable(True)
              scrollContent = QWidget(self.scrollArea)
              self.setAttribute(Qt.WA_DeleteOnClose)
              self.scrollLayout = QHBoxLayout(scrollContent)
              scrollContent.setLayout(self.scrollLayout)
              self.scrollLayout.setAlignment(Qt.AlignLeft)
              self.scrollArea.setWidget(scrollContent)
              self.setAcceptDrops(True)

              self.setCheckable(True)
              
              if (df is None):
                     df =  pd.DataFrame()
              
              if (len(df)>0):
                     self.df = df.head(0)
                     df.loc[:,'rotated']=0
                     self.addWidgetsFromDF(df)
              self.df = df

              
              #self.setFixedHeight(300)
       def addWidgetsFromDF(self, groupdf):
              newdf = groupdf[groupdf['path'].isin(self.df['path'].tolist())==False]
              self.df = groupdf
              newdf = newdf[newdf['isdir']==False]
              for index_cat, row in  newdf.iterrows():
                     self.addWidgetFromRow(row)
              #self.window().updateActions() 
                             
       def addWidgetFromRow(self, row):
              if (row['isdir']!=True):
                      fileName = row['path']
                      
                      imageLabel =clickableLabel(fileName, row)               
                      imageLabel.setBackgroundRole(QPalette.Base)
                      imageLabel.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
                      imageLabel.setScaledContents(True)

                      if imageLabel.filetype.lower() in img_ext_list:
                             image = QImage(fileName)                                                    
                      else:
                             image = QImage('.\icons\word.svg')
                            #imageLabel.setFixedHeight(200)
                            #imageLabel.setFixedWidth(int(float(image.width())*200./float(image.height())))
                      image = image.scaledToHeight(600)
                      imageLabel.setPixmap(QPixmap.fromImage(image))
                      imageLabel.setToolTip(fileName)                      
                                           
                      imageLabel.setMaximumHeight(ceil(self.height()*0.8-self.scrollArea.horizontalScrollBar().height()))                     
                      self.addWidget(imageLabel)
                      
       def addWidget(self, widget):
              widget.group = self.name
              self.scrollLayout.addWidget(widget,0)
              self.docWidgets.append(widget)
       def removeWidget(self, widget, deleteFromList=True):
              self.scrollLayout.removeWidget(widget)
              if (deleteFromList) and (widget in self.docWidgets):
                     self.docWidgets.remove(widget)
              if (len(self.docWidgets)==0):
                     reply = QMessageBox.question(self, 'Внимание',
                                                   "Удалить группу документов "+self.name+"?", QMessageBox.Yes |
                                                   QMessageBox.No, QMessageBox.No)
                     if reply == QMessageBox.Yes:
                            self.window().scrollLayout.removeWidget(self)
                            self.window().docGroups.remove(self)
                            self.close()
                            self.window().updateActions()
       def setFixedHeight(self,height):
              super(QGroupBox, self).setFixedHeight(height)
              i = 0 
              while i < self.scrollLayout.count():
                     widget = self.scrollLayout.itemAt(i).widget()
                     i=i+1
                     if (self.scrollLayout!=None):
              #for widget in self.docWidgets:
                            widget.setFixedHeight(ceil(self.height()*0.8))
       def addWidgetBeforeWidget(self, exist_widget:clickableLabel, new_widget:clickableLabel):
              #self.scrollLayout.addWidget(new_widget)
              position = self.scrollLayout.indexOf(exist_widget)
              inner = ( new_widget.group ==self.name)
              new_widget.group = self.name
              
              widgetlist = list()
              widgetlist.append(new_widget)
              if (len(self.docWidgets)>0 and position!=-1):
                     i=position
                     while i<self.scrollLayout.count():
                            item = self.scrollLayout.itemAt(i)
                            if (item is not None):
                                   wid = item.widget()
                                   if (wid in self.docWidgets):
                                          widgetlist.append(wid)
                                   if inner:
                                          self.removeWidget(wid,True)
                                   else:
                                          self.removeWidget(wid,True)
                     self.scrollLayout.update()
              for widget in widgetlist:
                     self.addWidget(widget)
              self.scrollLayout.update()


       def get_df(self):
              newdf = self.df.head(0)
              for widget in self.docWidgets:
                 if (widget.metaObject().className()=='clickableLabel'):
                     widget.row['rotated'] = widget.rotated
                     newdf = newdf.append(widget.row)
                     print("Номер виджета "+widget.filename+' в группе '+self.name +" - "+str(self.scrollLayout.indexOf(widget)))
              newdf['doc_type'] = self.name       
              self.df = newdf
              return self.df
       def dragEnterEvent(self, e):
              e.accept()
       def dropEvent(self, e:QDropEvent):
              position = e.pos()
              x = position.x()
              widget:clickableLabel = e.source()
              main_window = widget.window()
              oldGroup:docQGroupBox = main_window.searchdocGroup(widget.group)
              oldGroup.removeWidget(widget, True)
              widget.group = self.name
              oldwidget = None

              if (self != widget):
                     oldwidget:clickableLabel = None
                     for wid in self.docWidgets:
                            if ((wid.pos().x()+wid.size().width())>x):
                                   oldwidget = wid
                     if (oldwidget is not None):      
                            print( widget.path+' -> '+oldwidget.path)
                            self.addWidgetBeforeWidget(oldwidget, widget)
                     else:
                            self.addWidget(widget)
                     e.accept()
              else:
                     e.ignore()
   

class ImageViewer(QMainWindow):
    docGroups = list()
    main_df = pd.DataFrame()
    selected_images = list()



    def __init__(self):
        super(ImageViewer, self).__init__()
        self.selected_images = list()
        self.setAcceptDrops(True)
        self.main_df=pd.DataFrame() 
        self.centralwidget = QWidget()
        self.printer = QPrinter()
        self.scaleFactor = 1.0  
        self.setWindowIcon(QIcon('.\icons\icon.svg'))
        
        #settings_load
        self.cat_df = pd.read_excel('.\\settings\\category.xlsx')
        
        
        #base_widget load
        
        hbox = QHBoxLayout(self)
        self.topleft = QFrame(self)
        self.topleft.setFrameShape(QFrame.StyledPanel)

        self.topright = QFrame(self)
        self.topright.setFrameShape(QFrame.StyledPanel)
        self.topright.setFixedWidth(0)
        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(self.topleft)
        splitter.addWidget(self.topright)
        #добавляем консоль
        console = QTextEdit(self)
        console.setFixedHeight(80)
        console.setReadOnly(True)
        c = Communicate()
        c.printText.connect(console.insertPlainText)
        sys.stdout = c
        
        splitter1 = QSplitter(Qt.Vertical)
        splitter1.addWidget(splitter)
        splitter1.addWidget(console)
        
        hbox.addWidget(splitter1)
        self.centralwidget.setLayout(hbox)

        
        #собираем правую панель
        grid = QGridLayout()
        self.topright.setLayout(grid)
        self.lbl = QLabel("Категория документа", self)
        self.lbl2 = QLabel("Печатать", self)
        grid.setAlignment(Qt.AlignTop)
        combo = QComboBox(self)
        combo.addItems(["ЕГРН", "Свидетельство о собственности",
                        "Fedora", "Arch", "Gentoo"])
        combo1 = QComboBox(self)
        combo1.addItems(["Да", "Нет"])
        grid.addWidget(self.lbl,0,0)
        grid.addWidget(combo,0,1)
        grid.addWidget(self.lbl2,1,0)
        grid.addWidget(combo1,1,1)

        
        
        listBox = QGridLayout(self)
        self.topleft.setLayout(listBox)
        self.scrollArea = QScrollArea(self.topleft)
        listBox.addWidget(self.scrollArea)
        self.scrollArea.setWidgetResizable(True)
        self.scrollContent = QWidget(self.scrollArea)
        self.scrollLayout = QVBoxLayout(self.scrollContent)
        self.scrollContent.setLayout(self.scrollLayout)
        self.scrollArea.setWidget( self.scrollContent)
        
        

        #self.docGroupscrollArea = QScrollArea(self.docGroup)
        #self.docGroupscrollArea.setWidgetResizable(True)

       # self.scrollLayout.addStretch(1)
        #self.scrollLayout.addWidget(docQGroupBox('test2'))
        #self.scrollLayout.addWidget(docQGroupBox('test3'))
        self.setCentralWidget( self.centralwidget)
        self.imageLabels=list()
        self.createActions()
        self.createMenus()
        self.createToolbar()

        self.setWindowTitle("АС Пакет документов")
        self.resize(1000, 700)
    
       
       
    def sortdfbycatdf(self, df,cat_df):
           if ('checkpriority' not in df):
                  df = df.merge(cat_df, left_on='doc_type', right_on='doc_type', how='left')
                  df.loc[((df['checkpriority']==1)&(df['ext'].isin(doc_ext_list))),'doc_type'] = 'п'+df[(df['checkpriority']==1)&(df['ext'].isin(doc_ext_list))]['real_type']

           df['colfromindex'] =  df.index
           df = df.sort_values(['priority','colfromindex'])
           df = df.reset_index(drop=True)
           df= df.drop('colfromindex', axis=1)
           
           return df
 
    def add_files(self):
           self.open_files(True)
           
           
    def open_files(self,add=False):
           fileNames, _ = QFileDialog.getOpenFileNames(self, "Выбрать пакет документов")
           i = 0

           if (len(fileNames)>0):
                  for fileName in fileNames:
                      doc_file = DocFile(fileName.lower())
                      if i==0:
                             filesdf = doc_file.childs_df                                            
                      else:
                             filesdf = filesdf.append(doc_file.childs_df)
                      i= i+1
                      
                  
                  filesdf = doc_file.predict_childs_type(filesdf)
                  filesdf = filesdf[filesdf['isdir']==False]  
                  filesdf = self.sortdfbycatdf(filesdf,self.cat_df)
                
                  if add:
                         self.main_df = self.main_df.append(filesdf,ignore_index=True)
                  else:
                         self.main_df = filesdf
                  self.main_df = self.sortdfbycatdf(self.main_df,self.cat_df)
                  self.reload_images()
           
    def searchdocGroup(self, docGroupname):
           if (len(self.docGroups)>0):
                  for docGroup in self.docGroups:
                         if (docGroup.name ==docGroupname ):
                                return docGroup
           return None 
    def sortdocGroup(self):
           self.main_df = self.get_result_df()
           if (len(self.docGroups)>0) and (self.cat_df is not None):
               docgroupspos = list()
               for docGroup in self.docGroups:
                      priority = self.cat_df[self.cat_df['doc_type']==docGroup.name]['priority'].min()
                      #if (docGroup.name in )
                      docgrouppos = [docGroup, priority]
                      docgroupspos.append(docgrouppos)
                      self.remDocGroup(docGroup, False, False)
                      self.updateActions()
                      
               self.docGroups = list()
               self.updateActions()
               docgroupspos = sorted(docgroupspos, key = lambda x: (x[1]))
               for dg in docgroupspos:
                      self.addDocGroup(dg[0])
                      self.updateActions()
               self.main_df = self.get_result_df()
               self.main_df = self.sortdfbycatdf(self.main_df,self.cat_df)
                                                       
    def ocr(self):
           if (len(self.docGroups)>0):
                  ocr_df = self.get_result_df()
                  docfile= DocFile('')
                  ocr_df = docfile.predict_childs_type(ocr_df, True, float(2))
                  self.main_df = ocr_df
                  self.reload_images()
           else:
                  QMessageBox.warning(self,'Пакет не загружен','В программу не загружен ни 1 документ')
       
                      #return
                      
    def get_result_df(self):
           result_df = self.main_df.head(0)
           #result_df = result_df.reset_index()

           for doc_group in self.docGroups:
                  if (doc_group.isChecked()):
                        result_df= result_df.append(doc_group.get_df())
           self.main_df = result_df
           return self.main_df
    def add_select_image(self, image):
           if (image in self.selected_images):
                  self.selected_images.remove(image)
           self.selected_images.append(image)
    
    def del_select_image(self, image):
           if (image in self.selected_images):
                  self.selected_images.remove(image)
    def unselect_images(self):
           if (len(self.selected_images)>0):
                  for imagelabel in self.selected_images:
                         imagelabel.unselect_image()
    
    
    def select_from_widget_to_widget(self, widget1, widget2):
        docGroup1 = self.searchdocGroup(widget1.group)
        docGroup2 = self.searchdocGroup(widget2.group) 
        widget1pos =  docGroup1.scrollLayout.indexOf(widget1)
        widget2pos =  docGroup2.scrollLayout.indexOf(widget2)
        pos1=self.scrollLayout.indexOf(docGroup1)
        pos2=self.scrollLayout.indexOf(docGroup2)
        widgetmin = widget1
        widgetmax = widget2
        #docGroupmin = docGroup1
        #docGroupmax = docGroup2
        if ((pos1>pos2) or (widget1pos>widget2pos and pos1==pos2)):
               widgetmin = widget2
               widgetmax = widget1
               #docGroupmin = docGroup2
               #docGroupmax = docGroup1
                      
        i = 0
        pos_max = self.scrollLayout.count()
        start = False
        stop = False 
        while i <pos_max:

               item= self.scrollLayout.itemAt(i)
               if item is not None:
                      docGroup = item.widget()
                      if docGroup is not None:
                             if (docGroup.metaObject().className()=='docQGroupBox'):                     
                                    pos_end = docGroup.scrollLayout.count()       
                                    j = 0
                                    while (j<pos_end):
                                           item1= docGroup.scrollLayout.itemAt(j)
                                           if item1 is not None:
                                                  widget = item1.widget()
                                                  if (widget is not None):
                                                         if (widget.metaObject().className()=='clickableLabel'):
                                                                if (widget.path==widgetmin.path):
                                                                       start = True
                                                                if (widget.path==widgetmax.path):
                                                                       stop = True
                                                                if start:
                                                                       print(widget.path)
                                                                       widget.select_image()
                                                                if stop:
                                                                       return
                                           j=j+1
                      
                  
               i= i+1
           
    def savepdf(self):
        df = self.get_result_df()
        self.main_df = df
        filelist  = self.main_df['path'].tolist()
        rotatedlist  = self.main_df['rotated'].tolist()
        filelist = list(zip(filelist, rotatedlist))
        fileName = QFileDialog.getSaveFileName(self, "Сохранение PDF","", "PDF (*.pdf);;Все файлы (*)")
        if fileName[0]!='':
               if (os.path.exists(fileName[0])):
                      os.remove(fileName[0])
               myPDF.files_to_Pdf(fileName[0],filelist)
               reply = QMessageBox.question(self, 'Файл сохранен',
                                                   "Открыть файл для просмотра?", QMessageBox.Yes |
                                                   QMessageBox.No, QMessageBox.Yes)
               if (reply==QMessageBox.Yes):
                      os.startfile(fileName[0])
                      
        #except:
        #       QMessageBox.warning(self,'Документ не сохранен', 'Документ не сохранен,ошибка сохранения')
    def clearLayout(self, layout):
           self.unselect_images()
           for i in reversed(range(layout.count())):
                  item = layout.itemAt(i)
                  if isinstance(item, QWidgetItem):
                         print("widget" + str(item))
                         item.widget().close()
                   # or
                   # item.widget().setParent(None)
                  elif isinstance(item, QSpacerItem):
                          print("spacer " + str(item))
                   # no need to do extra stuff
                  else:
                          print("layout " + str(item))
                          self.clearLayout(item.layout())
                  layout.dItem(item) 
           
                  
    def reload_images(self):
           self.unselect_images()
           #if (len(self.docGroups)>0):
           #       for docGroup in self.docGroups:
           #              self.remDocGroup(docGroup)
                  
                  
                  #self.scrollLayout.
                  #self.clearLayout(self.scrollLayout)
                  #self.updateActions()
                  #self.docGroups = list()
           groups = self.main_df['doc_type'].drop_duplicates().tolist()
           
           for group in groups:
                  groupdf = self.main_df[self.main_df['doc_type']==group]
                  foundedGroupDoc:docQGroupBox = self.searchdocGroup(group)
                  if (self.searchdocGroup(group)==None):
                         docGroup:docQGroupBox = docQGroupBox(group,groupdf)
                         self.addDocGroup(docGroup)
                  else:
                         foundedGroupDoc.addWidgetsFromDF(groupdf)
           self.sortdocGroup()
           
           #self.addWidget(scrollArea)
           
           self.scrollLayout.addStretch(1)
           self.scaleFactor = 1.0  
           self.savedocpdf.setEnabled(True)
           self.recognizedoc.setEnabled(True)
           self.fitToWindowAct.setEnabled(True)
           self.updateActions()
           #if not self.fitToWindowAct.isChecked():
           #           imageLabel.adjustSize()


    def print_(self):
        dialog = QPrintDialog(self.printer, self)
        if dialog.exec_():
            painter = QPainter(self.printer)
            rect = painter.viewport()
            size = self.imageLabel.pixmap().size()
            size.scale(rect.size(), Qt.KeepAspectRatio)
            painter.setViewport(rect.x(), rect.y(), size.width(), size.height())
            painter.setWindow(self.imageLabel.pixmap().rect())
            painter.drawPixmap(0, 0, self.imageLabel.pixmap())

    def zoomIn(self):
        self.scaleImage( self.scaleFactor+0.1)

    def zoomOut(self):
        self.scaleImage(self.scaleFactor-0.1)

    def normalSize(self):
        self.scaleFactor = 1
        self.scaleImage(self.scaleFactor)

    def fitToWindow(self):
        fitToWindow = self.fitToWindowAct.isChecked()
        self.scrollArea.setWidgetResizable(fitToWindow)
        if not fitToWindow:
            self.normalSize()

        self.updateActions()

    def about(self):
        QMessageBox.about(self, "About Image Viewer",
                "<p>The <b>Image Viewer</b> example shows how to combine "
                "QLabel and QScrollArea to display an image. QLabel is "
                "typically used for displaying text, but it can also display "
                "an image. QScrollArea provides a scrolling view around "
                "another widget. If the child widget exceeds the size of the "
                "frame, QScrollArea automatically provides scroll bars.</p>"
                "<p>The example demonstrates how QLabel's ability to scale "
                "its contents (QLabel.scaledContents), and QScrollArea's "
                "ability to automatically resize its contents "
                "(QScrollArea.widgetResizable), can be used to implement "
                "zooming and scaling features.</p>"
                "<p>In addition the example shows how to use QPainter to "
                "print an image.</p>")
    
       
       
       
       
    def change_cat_selected(self):
           if (len(self.selected_images)):
                  newcat = self.sender().text()
                  docGroup:docQGroupBox = self.searchdocGroup(newcat)
                  oldGroupFlag = True
                  if (docGroup is None):
                         docGroup:docQGroupBox = docQGroupBox(newcat, self.main_df.head(0))
                         docGroup.get_df()
                         self.addDocGroup(docGroup)
                         self.sortdocGroup()
                         oldGroupFlag = False
                  for imageLabel in self.selected_images:
                      imageLabel.user_check_needed = False
                      imageLabel.row['user_check_needed'] = False
                      if (imageLabel.group!=docGroup.name):
                              #if oldGroupFlag:
                              oldGroup:docQGroupBox = self.searchdocGroup(imageLabel.group)
                              oldGroup.removeWidget(imageLabel, True)
                                     #if (len(oldGroup.docWidgets)==0):
                                     #       self.window().scrollLayout.removeWidget(oldGroup)
                                     #       self.window().docGroups.remove(oldGroup)
                                     #       self.window().updateActions()
                              docGroup.addWidget(imageLabel)
                  #if (!oldGroupFlag):
                  #      self.sortdocGroup() 
                  self.unselect_images()
                  self.window().updateActions()
    
                  
    def addDocGroup(self, docGroup):
           docGroup.setFixedHeight(ceil(300*self.scaleFactor))
           self.scrollLayout.addWidget(docGroup)
           self.docGroups.append(docGroup)                                           
           self.updateActions()
           
    def remDocGroup(self,docGroup, close=False, remove=True):
           if (docGroup in self.docGroups):
                  self.scrollLayout.removeWidget(docGroup)
                  if (remove):
                         self.docGroups.remove(docGroup) 
                  if close:
                         docGroup.close()
                  self.updateActions()
    def deldoc(self):
        reply = QMessageBox.question(self, 'Внимание',
                                                   "Удалить выбранные документы из пакета?", QMessageBox.Yes |
                                                   QMessageBox.No,QMessageBox.Yes)
        if reply == QMessageBox.Yes:   
                      for imageLabel in self.selected_images:
                             docGroup = self.searchdocGroup(imageLabel.group)
                             self.main_df = self.main_df[self.main_df['path']!=imageLabel.path]
                           
                             docGroup.removeWidget(imageLabel, True)                             
                             imageLabel.close()
                      self.updateActions()
                      self.selected_images = list()

    def rotateselected(self, degree):
        for docGroup in self.docGroups:
               for imageLabel in docGroup.docWidgets:
                      if imageLabel.selected:
                             imageLabel.rotate(degree)
                      
    def rotateselected_right(self ):
        self.rotateselected(90)
    def rotateselected_left(self ):
        self.rotateselected(270)
        
    def rotateselected_flip(self ):
        self.rotateselected(180)
                                         
                 
    def createActions(self):
        self.openAct = QAction("&Открыть пакет...", self, shortcut="Ctrl+O",
                triggered=self.open_files)
        self.addfileAct = QAction("&Добавить файл в пакет...", self, shortcut="Ctrl+O",
                triggered=self.add_files)

        self.printAct = QAction("&Распечатать пакет...", self, shortcut="Ctrl+P",
                enabled=False, triggered=self.print_)

        self.exitAct = QAction(QIcon('./icons/exit.svg'),"В&ыход", self, shortcut="Ctrl+Q",
                triggered=self.close)

        self.zoomInAct = QAction("Увеличить (10%)", self, shortcut="Ctrl++",
                enabled=False, triggered=self.zoomIn)

        self.zoomOutAct = QAction("Уменьшить (10%)", self, shortcut="Ctrl+-",
                enabled=False, triggered=self.zoomOut)

        self.normalSizeAct = QAction("&Обычный размер", self, shortcut="Ctrl+S",
                enabled=False, triggered=self.normalSize)

        self.fitToWindowAct = QAction("&Fit to Window", self, enabled=False,
                checkable=True, shortcut="Ctrl+F", triggered=self.fitToWindow)

        self.aboutAct = QAction("&About", self, triggered=self.about)

        self.aboutQtAct = QAction("About &Qt", self,
                triggered=QApplication.instance().aboutQt)
        
        #self.selectImageAct = QAction("&Выбрать изображение", self, triggered=self.select_image)
        
        self.rotateSelectedImageRightAct = QAction(QIcon('./icons/object-rotate-right.svg'),'Повернуть выделенные изображения на 90 вправо', self, triggered=self.rotateselected_right)
        self.rotateSelectedImageRightAct.setShortcut('Ctrl+Right')
        self.rotateSelectedImageRightAct.setStatusTip('Повернуть выделенные изображения на 90 вправо')
        
        self.rotateSelectedImageLeftAct = QAction(QIcon('./icons/object-rotate-left.svg'),"&Повернуть выделенные изображения на 90 влево", self, triggered=self.rotateselected_left)
        self.rotateSelectedImageLeftAct.setShortcut('Ctrl+Left')
        self.rotateSelectedImageLeftAct.setStatusTip('Повернуть выделенные изображения на 90 влево')
        
        self.rotateSelectedImageFlipAct = QAction(QIcon('./icons/object-flip-vertical.svg'),"&Перевернуть выделенные изображения", self, triggered=self.rotateselected_flip)
        self.rotateSelectedImageFlipAct.setShortcut('Ctrl+Up')
        self.rotateSelectedImageFlipAct.setStatusTip('Перевернуть выделенные изображения')
        
        self.recognizedoc = QAction(QIcon('./icons/ocr.svg'),'Распознать пакет документов через OCR', self, enabled=False,triggered=self.ocr)
        self.recognizedoc.setShortcut('Ctrl+R')
        self.recognizedoc.setStatusTip('Обработать пакет документов')
        
        self.sortdoc = QAction(QIcon('./icons/sort.svg'),'Сортировать документы', self, enabled=True,triggered=self.sortdocGroup)
        self.sortdoc.setShortcut('Ctrl+Q')
        self.sortdoc.setStatusTip('Сортировать документы')
        
        self.savedocpdf = QAction(QIcon('./icons/savepdf.svg'),'Сохранить пакет документов в pdf', self, enabled=False, triggered=self.savepdf)
        self.savedocpdf.setShortcut('Ctrl+S')
        self.savedocpdf.setStatusTip('Сохранить пакет документов в pdf')
        
        self.delete_selected = QAction(QIcon('./icons/delete.svg'),'Удалить выделенные документы из пакет', self, triggered=self.deldoc)
        self.delete_selected.setShortcut('Del')
        self.delete_selected.setStatusTip('Удалить выделенные документы из пакета')
        self.selCatActions = list()
        
    def createMenus(self):
        self.fileMenu = QMenu("&Файл", self)
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addAction(self.addfileAct)
        self.fileMenu.addAction(self.printAct)
        self.fileMenu.addAction(self.savedocpdf)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)
        self.editMenu = QMenu("&Правка", self)
        rotateMenu = QMenu("&Повернуть выделенные",self)
        self.editMenu.addMenu(rotateMenu)
        rotateMenu.addAction(self.rotateSelectedImageLeftAct)
        rotateMenu.addAction(self.rotateSelectedImageRightAct)
        rotateMenu.addAction(self.rotateSelectedImageFlipAct)
        self.editMenu.addAction(self.delete_selected)

        self.selCatMenu = QMenu("&Сменить категорию", self)
        
        self.editMenu.addMenu(self.selCatMenu)
        
        self.categoriesGroup = self.cat_df[self.cat_df['priority']!=0]['doc_type_group'].drop_duplicates().dropna().tolist()
        self.categoriesGroup.sort()
        for category in self.categoriesGroup:
            actlist = self.cat_df[self.cat_df['doc_type_group']==category]['doc_type'].drop_duplicates().dropna().tolist()
            podcatMenu = self.selCatMenu
            if (len(actlist)>1):      
                   podcatMenu = QMenu(category, self)
                   self.selCatMenu.addMenu(podcatMenu)
            for act in  actlist:
               #catAct = QAction(QIcon('./icons/changegroup.svg'),act, self, triggered=self.change_cat_selected)
               catAct = QAction(act, self, triggered=self.change_cat_selected)
               podcatMenu.addAction(catAct)
               self.selCatActions.append(catAct)
        self.editMenu.menuAction().setEnabled(False)
        self.viewMenu = QMenu("&Вид", self)
        self.viewMenu.addAction(self.zoomInAct)
        self.viewMenu.addAction(self.zoomOutAct)
        self.viewMenu.addAction(self.normalSizeAct)
        self.viewMenu.addSeparator()
        self.viewMenu.addAction(self.fitToWindowAct)

        self.helpMenu = QMenu("&Помощь", self)
        self.helpMenu.addAction(self.aboutAct)
        self.helpMenu.addAction(self.aboutQtAct)

        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addMenu(self.editMenu)
        self.menuBar().addMenu(self.viewMenu)
        self.menuBar().addMenu(self.helpMenu)
        

        
        
    def createToolbar(self):
        toolbar = self.addToolBar('Main')
        toolbar.addAction(self.rotateSelectedImageLeftAct)
        toolbar.addAction(self.rotateSelectedImageRightAct)
        toolbar.addAction(self.rotateSelectedImageFlipAct)
        toolbar.addAction(self.recognizedoc)
        toolbar.addAction(self.sortdoc)
        
        toolbar.addAction(self.delete_selected)
        toolbar.addAction(self.savedocpdf)
    def updateActions(self):
        self.zoomInAct.setEnabled(not self.fitToWindowAct.isChecked())
        self.zoomOutAct.setEnabled(not self.fitToWindowAct.isChecked())
        self.normalSizeAct.setEnabled(not self.fitToWindowAct.isChecked())

    def scaleImage(self, factor):
        
        self.scaleFactor = factor
        standartheight = 300
        for docGroup in self.docGroups:
               docGroup.setFixedHeight(ceil(self.scaleFactor * standartheight))

        self.adjustScrollBar(self.scrollArea.horizontalScrollBar(), factor)
        self.adjustScrollBar(self.scrollArea.verticalScrollBar(), factor)

        self.zoomInAct.setEnabled(self.scaleFactor < 5.0)
        self.zoomOutAct.setEnabled(self.scaleFactor > 0.1)

    def adjustScrollBar(self, scrollBar, factor):
        scrollBar.setValue(int(factor * scrollBar.value()
                                + ((factor - 1) * scrollBar.pageStep()/2)))


if __name__ == '__main__':

    import sys
    
    app = QApplication(sys.argv)
    qt_translator = QTranslator()
    if ( qt_translator.load( "qt_ru.qm" )!=False ):
         app.installTranslator( qt_translator )
    imageViewer = ImageViewer()
    imageViewer.show()
    sys.exit(app.exec_())
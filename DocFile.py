# -*- coding: utf-8 -*-
'''
Created on Mon Oct 23 21:02:28 2017

@author: azazello
'''
import os
import sys
import subprocess
from multiprocessing.dummy import Pool as ThreadPool

import pandas as pd
from natsort import natsorted, ns

import PDFEditor
from my_tesseract import TesseractOCR
from DocumentTypeDetector import DocumentTypeDetector


pd.options.mode.chained_assignment = None

UNPACKABLE_FORMATS = ['.zip', '.7z', '.rar', '.rar', '.arj', '.pdf']
ARCHIVE_FORMATS = ['.zip', '.7z', '.rar', '.rar', '.arj']


def clear_salt(filename: str):
    """
    очищает от соли строку c именем файла. Соль - _[a-z0-9]{5}\.
    :param filename:
    :return:
    """
    result = filename

    if type(filename) is str:
        result = filename.replace(u'_[a-z0-9]{5}\.', u'\.')

    return result


class DocFile:
    """
    Класс файла с докумментом
    """
    # базовые свойства
    path = str()
    filename = str()
    folder = str()
    parent_path = str()
    ext = str()
    is_dir = False
    doc_type = str()
    predict_filename_type = str()
    predict_filename_type_coef: float
    predict_text_type = str()
    predict_text_type_coef: float
    predict_group_type: str
    user_check_needed = False
    # для папок и архивов
    contains_one_folder = False
    childs_df = pd.DataFrame()
    childs = list()
    is_unpacked = False
    is_multipage = False
    text = str()

    def __init__(self, path_):
        '''
        Инициализуем экземпляр класса
        Разбираем файл до запчастей
        Если архив - он будет рекурсивно распакован
        Если PDF в котором только изображения - разобран на станицы
        Все это прокаталагизируется и соберется в корнейвой экземпляр
        :param path_: путь к файлу
        '''

        if path_ == '' or not os.path.exists(path_):
            print('Нет такого файла/папки - ' + path_)
            return
        # для начала приведем путь к абсолютному
        self.path = os.path.abspath(path_)
        # расширение по умолчанию - пустое
        self.ext = ''
        # проверяем - а не с папкой ли мы имеем дело?
        self.is_dir = os.path.isdir(self.path)
        if not self.is_dir:
            # если не с папкой - задаем раширение
            self.ext = os.path.splitext(path_)[1]
            self.ext = self.ext.lower()
        # задаем абсолютный путь родительского каталога
        self.parent_path = os.path.abspath(os.path.split(self.path)[0])
        # задаем имя папки нахождения файла и очищаем от соли
        self.foldername = os.path.split(self.parent_path)[1]
        self.foldername = clear_salt(self.foldername)
        # задаем имя файла экземпляра и очищаем от соли
        self.filename = os.path.basename(os.path.splitext(path_)[0])
        self.filename = clear_salt(self.filename)
        # задаем категорию по умолчанию и коэффициент по умолчанию
        # категория по прогнозированию по имени файла
        # и коэффициент по ней же
        self.predict_filename_type = ''
        self.predict_filename_type_coef = float(-50)
        # категория по прогнозированию по тексту
        # и коэффициент по ней же
        self.predict_text_type = ''
        self.predict_text_type_coef = float(-50)
        # итоговую категорию проставляем 'unknow'
        self.doc_type = 'unknow'
        # это свойство пригодится для последовательной сортировки
        #  по группе документов со схожими именами
        self.predict_group_type = ''
        # текст - пустой
        self.text = ''
        # если папка
        if self.is_dir:
            # собираем детей
            self.childs = self.__get_childs()
            # собираем DataFrame из детей
            self.childs_df = self.__get_childs_df()
            # проверяем содержит ли папка только 1 другую папку
            # по факту проверяем а не результат ли распаковки архива в папку
            child_count = len(os.listdir(path_))
            self.contains_one_folder = False

            if child_count == 1:
                grandchild = path_ + '\\' + os.listdir(path_)[0]
                if os.path.isdir(grandchild):
                    self.contains_one_folder = True

        else:
            # если не папка
            # получаем df содержащий 1 строку с данными файла
            self.childs_df = self.get_df_row()
            # ставим флаги присущие папкам
            self.contains_one_folder = False
            # проверяем, можно ли этот файл распаковать
            is_unpackable = self.ext in UNPACKABLE_FORMATS
            self.is_unpacked = False
            if is_unpackable and not self.is_unpacked:
                # распаковываем файл
                dir_name = self.unarchive(delete_archives=False)
                # если распаковка удачна - делаем замену этого экземпляра
                # на экземпляр папки созданной распаковкой
                # сам архив нам более неинтересен
                if dir_name is not None:
                    self.__init__(dir_name)

    def get_child_by_path(self, path):
        """
        Ищет во всем дочерних экземплярах включая внуков и т.д.
        child с указанным путем.
        По себе не ищет.
        :param path:str путь к файлу
        :return:DocFile экземпляр соответсвующий данному пути или None
        """
        founded_child = None
        for child in self.childs:
            if child.path != path:
                founded_child = child.get_child_by_path(path)
                if founded_child is not None:
                    break
        return founded_child

    def print_prop(self):
        """
        Печатает свойства экземпляра класса. Использовался для отладки
        :return:
        """
        print('Path: ' + str(self.path) + ' filename: ' + str(self.filename) + ' foldername: ' + str(
            self.foldername) + ' ext: ' + str(self.ext) + ' isdir:' + str(self.is_dir) + ' contains_one_folder: ' + str(
            self.contains_one_folder))

    def get_df_row(self):
        """
        Получает pandas DataFrame из 1 строки с свойствами экземляра класса.
        :return:DataFrame
        """
        # получает dataframe длиной аж в 1 строку, содежащую свойства класса.
        columns = ['path', 'parent_path', 'filename', 'foldername', 'ext',
                   'isdir', 'multipage', 'contains_one_folder', 'text',
                   'doc_type', 'user_check_needed', 'predict_filename_type',
                   'predict_filename_type_coef', 'predict_text_type',
                   'predict_text_type_coef', 'predict_group_type']
        data = [[self.path, self.parent_path, self.filename, self.foldername,
                 self.ext, self.is_dir, self.is_multipage, self.contains_one_folder,
                 self.text, self.doc_type, self.user_check_needed,
                 self.predict_filename_type, self.predict_filename_type_coef,
                 self.predict_text_type, self.predict_text_type_coef, self.predict_group_type]]

        df = pd.DataFrame(data, columns=columns)
        return df

    def set_child_prop_from_df(self, predicted_df: pd.DataFrame = None):
        """
        Устанавливает свойства всем детям объекта в соответствии с отработкой
        модели предсказания (на вход нужно подавать DF после отработки
        функции predict_childs_type)
        :param self:
        :param predicted_df: DF после отработки функции predict_childs_type
        :return: True если значения установлена и False если произошел сбой
        """
        is_sucefull_run = False

        try:
            if predicted_df is None:
                predicted_df = self.childs_df
            self_df = predicted_df[predicted_df['path'] == self.path]

            if len(self_df) > 0:
                self_row = self_df.iloc[0]

                self.filename = self_row['filename']
                self.folder = self_row['foldername']
                self.doc_type = self_row['doc_type']
                self.predict_filename_type = self_row['predict_filename_type']
                self.predict_filename_type_coef = self_row['predict_filename_type_coef']
                self.predict_text_type = self_row['predict_text_type']
                self.predict_text_type_coef = self_row['predict_text_type_coef']
                self.user_check_needed = self_row['user_check_needed']
                self.text = self_row['text']

                childs_count = len(self.childs)

                if childs_count > 0:
                    child_error = False

                    for child in self.childs:

                        if not child.set_child_prop_from_df(predicted_df):
                            child_error = True

                    is_sucefull_run = child_error

        except:
            print('Не удалось установить параметры для объекта: ' + self.path)
            error_text = sys.exc_info()[1]
            print(error_text)

        return is_sucefull_run

    def set_childs_filename_type(self, check_coef: float):
        """
        Устанавливает тип документа для детей экземпляра.
        Если у дочернего экземпляра категория не определена, или категория
        определена с меньшей точностью - меняет тип на родительский.
        Работает рекурсивно
        :type check_coef: float Коэффициент точности модели
        :return: True если удачно, False - если нет
        """
        # проверяем есть ли дети (только уровень -1) если есть
        # - проверяем установлен ли правильно тип (неопознанное ставим по категории родителя)
        is_set_sucefull = False
        childs_count = len(self.childs)

        # сравниваем коэффициент предсказания с проверочным, если ниже -
        # задаем тип документа unknow и коэффициент -50.
        if self.predict_filename_type_coef < check_coef:
            self.predict_filename_type = 'unknow'
            self.predict_filename_type_coef = -50.

        # Если детей нет, на этом стоп.
        if childs_count == 0:
            return True

        try:
            for child in self.childs:
                # Меняем категорию детей, у которых категория не опознана,
                # по категории родителя. Это проводится только по папкам,
                # потому анализ текста тут неактуален

                if (child.predict_filename_type == 'unknow' or
                        (child.predict_filename_type == self.predict_filename_type
                         and
                         child.predict_filename_type_coef < self.predict_filename_type_coef)):
                    child.predict_filename_type = self.predict_filename_type
                    child.predict_filename_type_coef = self.predict_filename_type_coef

                # ну и рекурсия по деткам
                child.set_childs_filename_type(check_coef)

        except:
            print('Не удалось установить тип для объекта: ' + self.path)
            is_set_sucefull = False
        return is_set_sucefull

    def extract_text(self, path_list):
        """
        Извлекает текст из файлов по списку путей
        :param path_list: список путей
        :return: список с текстом
        """

        def valuation_formula(path):
            try:
                print('Распознаю файл: ' + path)
                # извлекаю текст из файла
                text = TesseractOCR.get_text(path)
                return text

            except:
                print('Файл: ' + path + ' распознан с ошибками!')
                error_text = sys.exc_info()[1]
                print(error_text)
                return ''

        pool = ThreadPool(6)
        text = pool.map(valuation_formula, path_list)
        return text

    def predict_childs_type(self, childs_df=None, ocr=False, check_coef=float(-0.5)):
        """
        Функция предсказания категории документа в зависимости от его имени файла,
        папки, содержимого.
        Работает по схеме - определяет категорию по названию файла и
        папки, расширению. После чего всем, чему не определила проставляет
        категорию родительского обекта (папки) все что не смогла понять -
        в зависимости от параметра OCR - если  True распознает и определяет
        категорию по тексту, также выдает коэффициент по которому была
        определена категория(чем выше значение - тем больше точность)
        :param childs_df: pandas DataFrame c данными о файлах
        :param ocr: True или False - извлекать текст из документов или нет
        :param check_coef: float - коэффициент после котоого считается,
        что категория выбрана верно
        :return: pandas DataFrame c заполненными данными о типах документа

        """
        '''
    
        '''
        # загружаем детектор, если нужно с моделью для распознования файлов
        ddt = DocumentTypeDetector(True, ocr, True)
        # если DataFrame не задан - собираем из детей экземлпяра.
        if childs_df is None:
            if self.childs_df is None:
                original_df = self.__get_childs_df()
            else:
                original_df = self.childs_df
        else:
            original_df = childs_df
        # прогноняем модель по именам файлов

        predicted_df = ddt.detect_type_filename(original_df,
                                                'filename',
                                                'foldername',
                                                'ext',
                                                'predict_filename_type',
                                                'predict_filename_type_coef',
                                                True)

        # проставляем свойства экземпляру и всем его деткам
        self.set_child_prop_from_df(predicted_df)
        # модель не отрабатывает идеально, есть ситуации, когда по названию
        # папки можно понять что внутри, именно таким образом раставляем
        # категории там, где алгоритм сам отработал плохо
        # также проставляем по пороговому коэффициенту категорию unknow
        # если ниже коэффициент - считаем, что модель не отработала
        # тут отработка зависит от потребности в ocr.

        if not ocr:
            self.set_childs_filename_type(-100.)

        else:
            self.set_childs_filename_type(check_coef)

        # перечитываем DataFrame с детей

        predicted_df = self.__get_childs_df()
        # проставляем категорию документа по отработке модели по именам
        predicted_df.loc[:, 'doc_type'] = predicted_df['predict_filename_type']
        self.childs_df = predicted_df
        self.set_child_prop_from_df(predicted_df)

        if ocr:
            # если нужно зайдествовать извлечение текста:
            # собираем DataFrame с нераспознанными файлами или с файлами,
            # по которым категория определена не достаточно точно.
            unknow_df = predicted_df[
                (predicted_df['predict_filename_type'] == 'unknow')
                | (predicted_df['predict_filename_type_coef'] < check_coef)]
            unknow_count = len(unknow_df)
            if unknow_count > 0:
                # если детей много - заряжаем многопоточное извлечение
                # текста из файлов.
                file_list = unknow_df['path'].tolist()
                text = self.extract_text(file_list)
                unknow_df.loc[:,'text'] = text
                # прогноняем модель по тексту
                unknow_df = ddt.detect_type_text(unknow_df,
                                                 'text',
                                                 'predict_text_type',
                                                 'predict_text_type_coef',
                                                 True)
                # перезадаем тип документа
                unknow_df['doc_type'] = unknow_df['predict_text_type']
                # раскатываем изменения на всех детей и себя
                self.set_child_prop_from_df(unknow_df)
                self.childs_df = self.__get_childs_df()
        # запускаем алгоритм проставления категории по неразобранным
        self.set_type_by_filename_series()
        return self.childs_df

    def set_type_by_filename_series(self):
        """
        Задает тип документа для всех дочерних элементов экземпляра.
        Использует слудующий алгоритм:
        1. группирует все файлы -1 по имени файла
        2. перебирает каждую группу по алогритму
        3. группировка по определенному типу
        4. проставляет категорию для неопределенных по определенным
        Например для строки:NNXNNNYNNNZNNN, где N неопределенная,
        X - тип 1, Y тип 2, Z тип 3 решит:
        XXXXXXYYYYZZZZ.
        Логика отработки такая, потому что вероятнее всего категория
        документа будет определена по первым файлам документа.
        :return:DataFrame со скорректированым типом документа
        """
        dir_df = self.childs_df
        dir_df = dir_df.loc[(dir_df['isdir'] == True)
                            & (dir_df['contains_one_folder'] != True)]
        for index, row in dir_df.iterrows():
            # собираем группы имен файлов
            filename_group_df = self.childs_df[(self.childs_df['isdir'] == False)
                                               & (self.childs_df['parent_path'] == row['path'])]
            filename_groups = filename_group_df['filename'].drop_duplicates().tolist()
            # перебираем группы имен
            for filename_group in filename_groups:
                # формируем фрейм с дочерними файлами из той же именной группы
                files_df = filename_group_df[filename_group_df['filename']
                                             == filename_group]
                # собираем типы документов в этой именной группе
                filenames_cat = files_df['doc_type'].drop_duplicates().tolist()
                # если типов больше одного - запускаем обработчик
                # проставляем категорию документа по алгоритму: перебираем все
                # файлы по очереди, первое вхождение типа отличного от "unknow'
                # (тип1) считаем типом документов от начала списка до файла с
                # определенным типом (тип2), дальше от определенного, до определенного.
                if len(filenames_cat) > 1:
                    # задаем предыдущую категорию и счетчик
                    counter = 0
                    prev_cat = filenames_cat[0]
                    # перебираем строки в df с файлами
                    for index_cat, doc_row in files_df.iterrows():
                        # если первая катгория - unknow - добавляем счетчик
                        # и меняем ее на следующую.
                        if prev_cat == 'unknow':
                            counter = counter + 1
                            prev_cat = filenames_cat[counter]
                        # проставляем категорию для данного файла и ставим чек
                        # на проверке пользователя, если категория файла не определена.
                        # находим экземпляр, которому нужно поправить свойства
                        file_path = doc_row['path']
                        if doc_row['predict_filename_type'] == 'unknow':
                            doc_row['doc_type'] = prev_cat
                            doc_row['user_check_needed'] = True
                        else:
                            prev_cat = doc_row['doc_type']
                        # вносим результаты в DataFrame.

                            files_df.loc[files_df['path'] == doc_row['path'],
                                         'doc_type',] = doc_row['doc_type']
                            files_df.loc[files_df['path'] == doc_row['path'],
                                         'user_check_needed'] = doc_row['user_check_needed']

                else:
                    # если категория определена только 1 по группе файлов
                    # проставляем эту категорию всем остальным.
                    if filenames_cat[0] != 'unknow':
                        files_df.loc[:, 'user_check_needed'] = True
                    files_df.loc[:, 'doc_type'] = filenames_cat[0]

                self.set_child_prop_from_df(files_df)
                self.childs_df = self.__get_childs_df()






    def __get_childs_df(self):
        """
        # функция сканирования и записи информации о файлах в DataFrame, добавляет скозной индекс
        :return:DataFrame c потомками
        """

        doc_files = self.get_df_row()
        if len(self.childs) > 0:

            for child in self.childs:
                doc_files = doc_files.append(child.__get_childs_df())

            doc_files = doc_files.drop_duplicates()

            if len(doc_files) > 1:
                doc_files = self.sort_df(doc_files)

        return doc_files

    def sort_df(self, df):
        clear_path = df['path'].str.replace(u'_[a-z0-9]{5}\.', u'\.')
        df.loc[:, 'path_clear'] = clear_path
        df = df.set_index('path_clear')
        try:
            df = df.reindex(index=natsorted(df.index, alg=ns.PATH))
            df.index = range(0, len(df), 1)
        except:
            df = df
        return df

    def __get_childs(self, filenames=None):
        """
        Собирает экземпляры DocFile из списка имен или из папки,
        если инициирован папкой.
        :param filenames:list(str) список из срок
        :return:DocFile[]- список экземпляров дочерних DocFile
        """
        #
        file_path = self.path
        doc_file = self
        childs = list()
        if (doc_file.is_dir or doc_file.is_multipage) and file_path != '':
            if filenames is not None:
                files = filenames
            else:
                files = os.listdir(file_path)

            i = 0
            childs_count = len(files)
            while i < childs_count:
                if filenames is None:
                    filename = file_path + '\\' + files[i]
                else:
                    filename = files[i]

                new_child = DocFile(filename)
                childs.append(new_child)
                i += 1

        return childs

    def unarchive(self, delete_archives=False):
        """
        распаковывает архив или pdf файл (постранично на изображения).
        работает с self.path
        :param delete_archives:bool удалять архив после распаковки.
        :return:str path к папке, в которую распакован файл
        """
        # если путь не пустой проверяем - а не файл ли это
        is_unpacked = False
        result_dir_name = self.path[:-len(self.ext)]
        if os.path.isfile(self.path):
            # проверяем архив или нет
            if self.ext in ARCHIVE_FORMATS:

                if not os.path.exists(result_dir_name):
                    # создаем папку для распаковки
                    os.mkdir(result_dir_name)

                try:
                    cmd = list()
                    cmd.append(os.path.abspath('./utils/7z/7zG.exe'))
                    cmd.append('x')
                    cmd.append(self.path)
                    cmd.append('-aoa')
                    cmd.append('-o{}'.format(result_dir_name))
                    subprocess.call(cmd)
                    is_unpacked = True

                except Exception:
                    e = sys.exc_info()[1]
                    print('Ошибка распаковки архива ' + str(e.args[0]))

                if delete_archives:
                    # удаляем сам архив
                    os.remove(self.path)
                is_unpacked = True

            if self.ext == '.pdf':
                # если PDF - разбираем его на запчасти
                try:
                    mypdf = PDFEditor.PDFReader()

                    if len(self.filename) > 0:
                        filename = os.path.basename(self.path)[:-len(self.ext)]
                    else:
                        filename = 'pdfimg'
                    # читаем первые 500 строк текста из PDF
                    child_files = mypdf.get_text(self.path, 500)
                    self.text = '\n'.join(child_files)
                    # если есть столько текста - значит PDF
                    # не просто из скан-образов состоит.
                    if len(self.text) < 500:
                        result_dir_name = result_dir_name + '_pdf'

                        if not os.path.exists(result_dir_name):
                            # создаем папку для распаковки
                            os.mkdir(result_dir_name)
                        # излекаем изображения в папку
                        mypdf.get_pages(self.path,
                                        0,
                                        result_dir_name,
                                        filename,
                                        True)

                        is_unpacked = True
                    else:
                        is_unpacked = True
                except:
                    print('Error Unpack')
                    error_text = sys.exc_info()[1]
                    print(error_text)

            if delete_archives:
                # удаляем сам архив
                os.remove(self.path)

            self.is_unpacked = is_unpacked
            if is_unpacked:
                return result_dir_name
            else:
                return None
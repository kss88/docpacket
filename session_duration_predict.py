import pandas
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt


pd1 = pandas.read_csv('data.csv',delimiter=',')
pd11 = pd1[pd1['duration']<90]
pd11['val']= pd11['id']/pd11['id']
pd2:pandas.DataFrame = pandas.pivot_table(pd11,columns=['task_type_id'],values='val',index=['session_update_id', 'duration', 'product_type_id'])

pd2 = pd2.fillna(0)
pd3 = pd2.reset_index()
corr_tab = pd3.corr()
x_train = pd3
x_train = x_train.drop(columns=['session_update_id','duration'])
x_train = x_train.drop(columns=['product_type_id'])
y_train = pd3['duration']
offset = int(len(x_train)*0.9)

# Fit regression model
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import explained_variance_score as r2


for depth in range (1,2000,100):
    clf = RandomForestRegressor(max_depth=depth,n_jobs=-1)
    clf.fit(x_train[:offset], y_train[:offset])
    y_ = clf.predict(x_train[offset:])
    print('R^2 for ',depth,' depth: ', r2(y_train[offset:],y_))

from sklearn.dummy import DummyRegressor
clf  = DummyRegressor()
clf.fit(x_train[:offset], y_train[:offset])
print('R^2 for Ridge with alpha=1: ', clf.score(x_train[offset:],y_train[offset:]))


from sklearn.linear_model import LinearRegression

clf = LinearRegression()
clf.fit(x_train[:offset], y_train[:offset])
intercept = clf.intercept_
print('R^2 for Ridge with alpha=1: ', clf.score(x_train[offset:],y_train[offset:]))

clf = RandomForestRegressor(max_depth=3, n_jobs=-1)
clf.fit(x_train[:offset], y_train[:offset])
y_ = clf.predict(x_train[offset:])
print('R^2 for ', 6, ' depth: ', r2(y_train[offset:], y_))

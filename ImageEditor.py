# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 21:15:10 2017

@author: sergey kozlov
"""

import sys

from PIL import Image


# обработка изображений
class ImageEditor:

    @classmethod
    def open_image(cls, image_filename):
        """
        Открываем изображение
        :param image_filename:
        :return: None если открыть не удалось, PIL.image - если удалось
        """
        try:
            image = Image.open(image_filename, 'r')
            image.load()
            return image

        except IOError:
            print('Файл ' + image_filename + ' отсутсвует либо поврежден!')
            error_text = sys.exc_info()[1]
            print(error_text)
            return None

    @classmethod
    def save_image(cls, image: Image, path_to_save_image):
        """
        Сохраняем изображение
        :param path_to_save_image: путь для сохранения
        :param image - Объект PIL.image
        :return: True - если удалось, False если нет
        """
        try:
            image = image.save(path_to_save_image)
            return True
        except IOError:
            print('Файл ' + path_to_save_image + ' не сохранен!')
            error_text = sys.exc_info()[1]
            print(error_text)
            return False

    @classmethod
    def resize_fit_image(cls, img: Image, width=1240, height=1754):
        """
        Изменение размера изображения до указанных ширины и высоты,
        пропорции сохраняются! Подгонка идет по высоте/ширине, чтобы
        вписаться в заданный размер. Конвертер используется BILINEAR
        По умолчанию задан размер а4 150dpi

        :param img:PIL.Image
        :param width:int новая ширина
        :param height:int новая высота
        :return: img:PIL.Image с новым размером
        """

        img_width, img_height = img.size

        if img_width > img_height:
            height, width = width, height

        w_percent = width / float(img_width)
        fitted_height = int((float(img_height) * float(w_percent)))
        if fitted_height > height:
            w_percent = (height / float(fitted_height))
            fitted_width = int((float(width) * float(w_percent)))
            img = img.resize((fitted_width, height), Image.BILINEAR)
        else:
            img = img.resize((width, fitted_height), Image.BILINEAR)

        return img

    @classmethod
    def rotate_image(cls, image, degree_num):
        """
        Поворачивает изображение на требуемое количество градусов
        :param image: PIL.Image
        :param degree_num: int - градусы
        :return: PIL.Image, повернутый
        """
        degree_num_add = 0
        # поворачиваем полотно вместе с изображением
        if degree_num < 0:
            degree_num = degree_num + 360

        if 45 < degree_num <= 135:
            image = image.transpose(Image.ROTATE_270)
            degree_num_add = degree_num - 90

        elif 135 < degree_num <= 225:
            image = image.transpose(Image.ROTATE_180)
            degree_num_add = degree_num - 180

        elif 225 < degree_num <= 315:
            image = image.transpose(Image.ROTATE_90)
            degree_num_add = degree_num - 270

        elif 315 < degree_num <= 360:
            degree_num_add = degree_num - 360

        # докручиваем изображение
        image = image.rotate(degree_num_add)
        return image

    @classmethod
    def rotate_image_file(cls, file_path, degree_num):
        """
        Поворачивает изображение на требуемое количество градусов
        :param image: PIL.Image
        :param degree_num: int - градусы
        :return: PIL.Image, повернутый
        """
        rotate_sucefull = True
        image = cls.open_image(file_path)
        if image is not None:
            image = cls.rotate_image(image, degree_num)
            cls.save_image(image, file_path)


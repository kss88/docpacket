# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 21:10:08 2017

@author: azazello
"""
import sys
import os
import subprocess

import pytesseract
from numpy import ceil
from PIL import Image

from DocConverter import DocConverter
from ImageEditor import ImageEditor
import PDFEditor

class TesseractOCR():
    """
    Класс работы с Tesseract OCR
    """
    command = os.path.abspath('./utils/Tesseract-OCR/tesseract.exe')
    @classmethod
    def get_rotation_info(cls, filename):
        """
        Получить информацию о повороте изображения (текста на нем)

        :param filename: path файла
        :return: градусы для поворота
        """
        arguments = ' %s - -psm 0 -l rus'
        i = 0
        # цикл гоняет tesseract, если тот не может определиться с положением
        # изображения, крутим максимум 2 раза.
        while i < 2:
            i += 1
            # прогноняем tesseract
            try:
                stdoutdata = subprocess.getoutput('\"'
                                                  + cls.command
                                                  + '\"'
                                                  + arguments
                                                  % ('\"'+filename+'\"'))
                degrees = None
                # находим в выводе инфо о градуса
                for line in stdoutdata.splitlines():
                    info = 'Orientation in degrees: '
                    # если tesseract опредилил - выдаем градусы.
                    if info in line:
                        degrees = -float(line.replace(info, '').strip())
                        return degrees

                # если нет - поворачиваем файл на 90 градусов
                img = ImageEditor.open_image(filename)
                img = img.transpose(Image.ROTATE_90)
                # если файл в маленьком раззрешении - увеличиваем, особенность
                # tesseract - просто увеличив разрешение можно улучшить качество
                if max(img.size) < 2000:
                    img = img.resize((img.size[0]*2,
                                      img.size[1]*2),
                                     Image.ANTIALIAS)

                if ImageEditor.save_image(img, filename) is False:
                    return 0
            except Exception:
                print('Возникла проблема с Tesseract-OCR')
                error_text = sys.exc_info()[1]
                print(error_text)

        return degrees

    @classmethod
    def fix_rotation(cls, filename):
        """
        Поворачиваем изображение, на полученные от тессеракт градусы
        :param cls:
        :param filename:
        :return: PIL.image повернутый по указанию Tesseract
        """
        degrees = cls.get_rotation_info(filename)
        if degrees != 0 and degrees is not None:
            return ImageEditor.rotate_image_file(filename, degrees)


    @classmethod
    def get_text_from_image_file(cls, filename):
        """
        Извлекает русский текст из файла с изображением
        :param filename:
        :return:str
        """
        pytesseract.pytesseract.tesseract_cmd = cls.command
        cls.fix_rotation(filename)

        image = ImageEditor.open_image(filename)
        dpi = ceil(max([image.size[0], image.size[1]]) / 8.27)
        config = '--psm 3 --oem 3 -c image_default_resolution=' + str(dpi)
        if image is not None:
            text = pytesseract.image_to_string(image,
                                               lang='rus',
                                               boxes=False,
                                               config=config)
        return text

    @classmethod
    def get_text(cls, filename):
        """
        Извлекает текст из файла
        :param filename:str путь к файлу
        :return:str текст
        """

        if os.path.exists(filename):
            extracted_text = ''
            file_ext = os.path.splitext(filename)[1].lower()

            word_ext_list = ['.docx', '.doc', '.rtf', '.odt']
            img_ext_list = ['.jpg', '.jpeg', '.bmp', '.png']

            # если документ читаем текст
            if file_ext in word_ext_list:
                extracted_text = DocConverter.get_text_from_doc(filename)

            # если pdf - извлекаем текст
            if file_ext == '.pdf':
                extracted_text = PDFEditor.PDFReader.get_text(filename)

            # если изображения - распознаем
            if file_ext in img_ext_list:
                print('Обрабатываю файл: ' + filename)
                gray = ImageEditor.open_image(filename)

                # если изображение маленькое или большое - меняем размер
                if gray.size[0] < 1200 or gray.size[0] > 1500:
                    print('Меняю размер..')
                    gray = ImageEditor.resize_fit_image(gray, 1800, 1800)
                # обесцвечиваем
                gray = gray.convert('L')
                # сохраняем
                ImageEditor.save_image(gray, filename)

                print('Распознаю файл')
                extracted_text = cls.get_text_from_image_file(filename)

                return extracted_text

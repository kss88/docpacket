# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 21:15:10 2017

@author: sergey kozlov
"""
import os
import sys
import io
import math

from PIL import ImageSequence
from binascii import b2a_hex
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument, PDFNoOutlines
from pdfminer.pdfpage import PDFPage, PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBox, LTTextLine, LTFigure, LTImage
from fpdf import FPDF
from PyPDF2 import PdfFileReader, PdfFileWriter

from DocConverter import DocConverter
from ImageEditor import ImageEditor


# Модуль обработки PDF и конвертации файлов (jpg, gif, png, tiff, doc, docx, odt)

def del_file(file_path):
    """
    Удаляет файл по пути, с отработкой ошибок
    :param file_path: путь к файлу
    :return: True or False
    """

    try:
        os.remove(file_path)
        return True

    except IOError:
        print('Не могу удалить файл ' + file_path)
        error_text = IOError.args[1]
        print(error_text)
        return False


def get_temp_path(path):
    """
    Функция получения временного пути для файла
    :param path: путь файла
    :return: путь для временного файла (без расширения)
    """
    base_filename = os.path.splitext(path)[0]
    base_filename_ext = os.path.splitext(path)[1][1:]
    temp_file_path = base_filename + '_' + base_filename_ext
    return temp_file_path


class PDFReader:
    """

    Класс обработки PDF через PDF miner


    """
    # обнуляем счетчик сохраненных изображений
    img_num = 0

    def with_pdf(self, pdf_doc, fn, pdf_pwd, save_img, *args):
        """Открывает PDF документ и применяет функцию, возвращающую результат

        :param pdf_doc: str путь к PDF документу
        :param fn: функция для применения к файлу
        :param pdf_pwd: str пароль от PDF файла
        :param save_img: bool сохранять изображения или нет
        :param args: прочие аргументы
        :return: результат обработки функции
        """

        result = None
        try:
            # открываем файл для чтения в бинарном режиме
            fp = open(pdf_doc, 'rb')
            # Создаем объект парсер документа
            parser = PDFParser(fp)
            # Создаем объект документа с его стрктурой
            doc = PDFDocument(parser)
            # подключаем парсер к документу
            parser.set_document(doc)
            # если нужна обработка файлов с паролем
            # doc.initialize(pdf_pwd)

            if doc.is_extractable:
                # применяем функцию и получаем результат
                result = fn(doc, save_img, *args)

            # закываем файл
            fp.close()
        except IOError:
            # если есть проблемы с чтением/записью
            pass
        return result

    # Извлечение изображений

    def write_file(self, folder, filename, filedata, flags='w'):
        """запись данных файла

        :param folder: папка для записи файла
        :param filename: имя файла
        :param filedata: массив байтов
        :param flags: 'w' текста, 'wb' бинарный , 'a' после 'w' для дозаписи
        :return: True если запись удачна и False если нет
        """
        result = False
        if os.path.isdir(folder):
            try:
                file_obj = open(os.path.join(folder, filename), flags)
                file_obj.write(filedata)
                file_obj.close()
                result = True
            except IOError:
                pass
        return result

    def determine_image_type(self, stream_first_4_bytes):
        """Функция определения типа изображения по первым байтам

        :param stream_first_4_bytes: bytes[] первые 4 байта файла
        :return: str - расширение файла
        """

        file_type = None
        bytes_as_hex = b2a_hex(stream_first_4_bytes)
        if bytes_as_hex.startswith('ffd8'.encode()):
            file_type = '.jpeg'
        elif bytes_as_hex == '89504e47':
            file_type = '.png'
        elif bytes_as_hex == '47494638':
            file_type = '.gif'
        elif bytes_as_hex.startswith('424d'.encode()):
            file_type = '.bmp'
        return file_type

    def save_image(self, lt_image, page_number, images_folder, image_name):
        """Фунция сохранения изображения lt_image в папаку images_folder

        :param lt_image: объект lt_image
        :param page_number: int номер страницы
        :param images_folder: str путь для извлечения изображение
        :param image_name: ,базовое имя для изображений (к которому добавляется расширение и номер)
        :return: если успешна отработка - возращает имя сохраненного файла
        """

        result = None
        if lt_image.stream:
            file_stream = lt_image.stream.get_rawdata()
            if file_stream:
                file_ext = self.determine_image_type(file_stream[0:4])
                if file_ext:
                    file_name = ''.join([image_name, str(self.img_num), file_ext])
                    if self.write_file(images_folder, file_name, file_stream, flags='wb'):
                        result = file_name
                        self.img_num = self.img_num + 1
        return result

    # Извлечение текста

    def to_bytestring(self, s, enc='utf-8'):
        """Конвертация в utf-8,мпропускается, если s массив байтов.

        :param s: текст/массив байтов
        :param enc: кодировка
        :return: массив байтов строки
        """
        if s:
            if isinstance(s, str):
                return s
            else:
                return s.encode(enc)

    def update_page_text_hash(self, h, lt_obj, pct=0.2):
        """Использует bbox x0,x1 значения внутри pct% для создания списков связанного текста внутри хэша"""

        x0 = lt_obj.bbox[0]
        x1 = lt_obj.bbox[2]

        key_found = False

        for k, v in h.items():
            hash_x0 = k[0]
            if (x0 >= (hash_x0 * (1.0 - pct))) and ((hash_x0 * (1.0 + pct)) >= x0):
                hash_x1 = k[1]
                if (x1 >= (hash_x1 * (1.0 - pct))) and ((hash_x1 * (1.0 + pct)) >= x1):
                    # текст в этом LT* находится в той же колонке
                    key_found = True
                    v.append(self.to_bytestring(lt_obj.get_text()))
                    h[k] = v

        if not key_found:
            # это новая колонка
            h[(x0, x1)] = [self.to_bytestring(lt_obj.get_text())]

        return h

    def parse_lt_objs(self, lt_objs, page_number, images_folder, image_name, save_image, text=[]):
        """Перебираем лист LT объектов, собираем текст или изображения в каждом

        :param lt_objs: список объектов
        :param page_number: номер страницы
        :param images_folder: папка для сохранения изображений
        :param image_name: базовое имя для изображения
        :param save_image: bool сохранять изображения или нет
        :param text: список с извлеченными строками текста
        :return:
        """
        text_content = []

        page_text = {}  # k=(x0, x1) of the bbox, v=list of text strings within that bbox width (physical column)
        for lt_obj in lt_objs:
            if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
                # текст,  упорядочение логически основано на ширине столбца
                page_text = self.update_page_text_hash(page_text, lt_obj)
            elif isinstance(lt_obj, LTImage) and save_image:
                # изображение, сохраняем и отмечаем это в тексте тэгом
                saved_file = self.save_image(lt_obj, page_number, images_folder, image_name)
                if saved_file:
                    # use html style <img /> tag to mark the position of the image within the text
                    text_content.append('<img src="' + os.path.join(images_folder, saved_file) + '" />')
                else:
                    print(sys.stderr + "error saving image on page" + page_number + lt_obj.__repr__)
            elif isinstance(lt_obj, LTFigure):
                # LTFigure трясем детей через рекурсию
                text_content.append(
                    self.parse_lt_objs(lt_obj, page_number, images_folder, image_name, save_image, text_content))

        for k, v in sorted([(key, value) for (key, value) in page_text.items()]):
            # сортировка текста страницы по хэшу (x0,x1 значний bbox'а),
            # разбирает текст по столбцам слева направо
            text_content.append(''.join(v))

        return '\n'.join(text_content)

    # Processing Pages

    def _parse_pages(self, doc, images_folder, image_name, save_img):
        """With an open PDFDocument object, get the pages and parse each one
        [this is a higher-order function to be passed to with_pdf()]"""
        rsrcmgr = PDFResourceManager()
        laparams = LAParams()
        device = PDFPageAggregator(rsrcmgr, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)

        text_content = []
        for i, page in enumerate(PDFPage.create_pages(doc)):
            interpreter.process_page(page)
            # receive the LTPage object for this page
            layout = device.get_result()
            # layout is an LTPage object which may contain child objects like LTTextBox, LTFigure, LTImage, etc.
            text_content.append(self.parse_lt_objs(layout, (i + 1), images_folder, image_name, save_img))

        return text_content

    def get_pages(self,
                  pdf_doc,
                  pdf_pwd='',
                  images_folder='..\tmp',
                  image_name='pdfimg',
                  save_img=True):
        """Process each of the pages in this pdf file and return
        a list of strings representing the text found in each page"""
        return self.with_pdf(pdf_doc,
                             self._parse_pages,
                             pdf_pwd,
                             *tuple([images_folder]),
                             image_name,
                             save_img)

    @staticmethod
    def get_text(filename: str, text_len=0):
        """Функция быстрого извлечения текста из документа.

        Извлекает из PDF документа текст, в случае, если указан параметр
        text_len - определенной длины (или весь, если длина больше),
        в противном - весь текст документа.
        Работает на основе библиотеки pdfminer.six

        :param filename:str path-like имя PDF файла для извлечения

        :param text_len:int длина извлекаемого текста
        (на каком количестве текста остановиться)
        0 - для извлечения всего текста

        :return: Извлеченный текст либо None, в случае возникновения ошибки.
        """

        password = ""
        extracted_text = ""
        try:
            # открываем файл для чтения в бинарном режиме
            fp = open(filename, "rb")
        except IOError:
            print("Ошибка при открытии файла: " + filename)
            error_text = sys.exc_info()[1]
            print(error_text)
            return None

        # Создаем объект парсер документа
        parser = PDFParser(fp)

        # Помещаем в объект документа парсер, пароль как параметр обязателен
        document = PDFDocument(parser, password)

        # Проверяем читаем ли документ
        if not document.is_extractable:
            raise PDFTextExtractionNotAllowed

        # Создаем объект PDFResourceManager он содержит весь содержимый контект (изображения и т.д.)
        resource_manager = PDFResourceManager()

        # задаем параметры для анализа
        la_params = LAParams()

        # Создаем объект  PDFDevice для извлечения объектов со страницы.
        device = PDFPageAggregator(resource_manager, laparams=la_params)

        # Интерпретер для извлечения объектов со страницы, подключаем к PDFResourceManager
        interpreter = PDFPageInterpreter(resource_manager, device)

        # Перебираем контент страница за страницей
        for page in PDFPage.create_pages(document):
            # Страница помещается в объект PDFDocument
            interpreter.process_page(page)
            # PDFDevice получает слой от PDFPageInterpreter
            layout = device.get_result()
            # Пока на слое не закончатся LT objects мы извлекаем из них тектс,
            # нас интересуют только LTTextBox and LTTextLine.
            # Если задан параметр text_len - выгрузка прекратится,
            # как только длина извлеченного текста превысит text_len.
            for lt_obj in layout:
                if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
                    extracted_text += lt_obj.get_text()
                    if (text_len != 0) and (len(extracted_text) > text_len):
                        fp.close()
                        return extracted_text

        # закрываем файл и возвращаем текст
        fp.close()
        return extracted_text


class PDFCollector:
    """
    Класс для сбора PDF в один файл.
    """

    @classmethod
    def get_stream_from_file(cls, file_path, should_delete_file):
        """
        Читаем файл в bytes
        :param file_path: путь к файлу
        :param should_delete_file: bool удалять или нет исходный файл
        :return: None если не прочитал, bytes если прочитал
        """
        pdf_stream = None

        try:
            pdf_file = open(file_path, 'rb')
            pdf_stream = io.BytesIO(pdf_file.read())
            pdf_file.close()

        except IOError:
            print('Файл ' + file_path + ' не прочитан')

        finally:
            if should_delete_file:
                del_file(file_path)

        return pdf_stream

    @classmethod
    def get_pixels_size(cls, unit='mm', width=210, height=297, dpi=150):
        """
        Получает размер полотна в пикселях из заданного
        :param unit: единица измерения (pt, mm, in, cm)
        :param width: ширина (int)
        :param height: высота (int)
        :param dpi: DPI (int)
        :return: [width, height] в пикселях
        """

        ratio = float(1)

        if unit == 'mm':
            ratio = float(dpi) / 25.4
        elif unit == 'cm':
            ratio = float(dpi) / 2.54
        elif unit == 'in':
            ratio = float(dpi)

        new_width = int(math.ceil(width * ratio))
        new_height = int(math.ceil(height * ratio))

        return [new_width, new_height]

    @classmethod
    def get_pdf_streams_from_image(cls, image_filename,
                                   degree_num=0,
                                   unit='mm',
                                   page_width=210,
                                   page_height=297,
                                   dpi=150,
                                   should_delete_temp_file=True):
        """
        Конвертирует в PDF изображения: jpg, bmp, tiff, png, gif.
        В том числе многостраничные. Сама определеяет по размерам
        изображения формат страницы (портрет или альбомный)

        :param image_filename: путь к файлу с изображением
        :param degree_num: количество градусов для поворота изображения
        :param unit: единица измерения размера страницы (pt, mm, in, cm)
        :param page_width: ширина странцы в выбранной единице измерения
        :param page_height: высота странцы в выбранной единице измерения
        :param dpi: DPI
        :param should_delete_temp_file: bool удалять или нет временные файлы
        :return: массив байтов с PDF файлом из файла
        None - если не удалось создать PDF
        """
        # загружаем изображение
        image = ImageEditor.open_image(image_filename)

        if image is None:
            return None

        [image_width, image_height] = cls.get_pixels_size(unit, page_width,
                                                          page_height,
                                                          dpi)

        # создаем объект PDF для сохранения в него изображений
        pdf = FPDF(unit=unit, format=[page_width, page_height])

        # сохраняем все фреймы в отдельные файлы
        frames_path_list = list()
        [frames_path_list,
         new_image_width,
         new_image_height] = cls.save_resized_rotated_frames_from_image(image,
                                                                        image_filename,
                                                                        degree_num,
                                                                        image_width,
                                                                        image_height)

        # добавляем страницы
        for frame_path in frames_path_list:
            cls.add_pdf_page_from_image(pdf,
                                        new_image_width,
                                        new_image_height,
                                        frame_path,
                                        page_width,
                                        page_height)
            if should_delete_temp_file:
                del_file(frame_path)

        # возвращаем документ как массив байтов
        pdf_bytes = bytes(pdf.output(dest='S'), 'latin1')
        pdf_stream = io.BytesIO(pdf_bytes)
        return pdf_stream

    @classmethod
    def add_pdf_page_from_image(cls,
                                pdf,
                                image_width,
                                image_height,
                                image_filename,
                                page_width,
                                page_height):
        """
        Добавляет страницу в объект FPDP, содержащую изобображение.
        :param pdf: объект image_height
        :param image_width: ширина изображения
        :param image_height: высота изображени
        :param image_filename: имя файла, находящегося в PIL
        :param page_width: ширина страницы в ед измерения страницы
        :param page_height: высота страницы в ед измерения страницы
        :return: объект FPDP, с добавленной страницей-изображением
        """

        [pdf_width, pdf_height] = [page_width, page_height]
        if image_width > image_height:
            pdf.add_page('L')
            [pdf_width, pdf_height] = [page_height, page_width]
        else:
            pdf.add_page('P')

        pdf.image(image_filename, 0, 0, w=pdf_width, h=pdf_height)
        return pdf

    @classmethod
    def save_resized_rotated_frames_from_image(cls, image,
                                               base_path,
                                               degree_num,
                                               image_width,
                                               image_height,
                                               convert_to_gray=True):
        """
        Вытаскивает из объекта PIL.Image список изображений.
        Сохраняет их в папку исходного изображения.
        Поворачивает на градус и делает ресайз
        Еще и обесцвечивает :-)
        :param image: объект PIL.Image
        :param base_path: путь для сохранения файлов
        :param degree_num: градус на который надо повернуть
        :param image_width: ширина для ресайза
        :param image_height: высота для ресайза
        :param convert_to_gray: bool преобразовать в ЧБ или нет
        :return: image_list  список сохраненных изображений
        """

        frames_path_list = list()
        # задаем базовый путь для сохранения
        base_filename = get_temp_path(base_path)
        # сохраняем изображения циклом по фреймам
        index = 1
        new_image_width = image_width
        new_image_height = image_height
        for frame in ImageSequence.Iterator(image):
            iter_filename = base_filename + str(index) + '.png'
            # обесцвечиваем
            if convert_to_gray:
                frame = frame.convert('L')

            # поворачиваем изображение
            frame = ImageEditor.rotate_image(frame, degree_num)

            # изменяем размер
            frame = ImageEditor.resize_fit_image(frame,
                                                 image_width,
                                                 image_height)
            [new_image_width, new_image_height] = frame.size
            # сохраняем
            if ImageEditor.save_image(frame, iter_filename):
                frames_path_list.append(iter_filename)
            # индекс специально не внесен под условие - так
            # можно будет понять, что пропущены файлы
            index = index + 1

        return [frames_path_list, new_image_width, new_image_height]

    # сборка солянки файлов в pdf
    @classmethod
    def get_pdf_bytes_from_files(cls,
                                 files_list,
                                 page_unit='mm',
                                 page_width=210,
                                 page_height=297,
                                 dpi=150):
        """
        Функция формирования pdf из списка файлов
        обрабатываемые форматы:
        документы '.docx', '.doc', '.rtf', '.odt'
        изображения '.jpg', '.jpeg', '.bmp', '.tiff', '.tif', '.png'
        многостраничные тоже переваривает

        :param files_list: список из пар [название файла, градус для поворота изображения]
        :param page_unit: единица измерения размера страницы (pt, mm, in, cm)
        :param page_width: ширина странцы в выбранной единице измерения
        :param page_height: высота странцы в выбранной единице измерения
        :param dpi: DPI
        :return: True, если файл сохранен, False - если нет
        """
        files_list_len = len(files_list)

        if files_list_len < 1:
            return False

        doc_ext_list = ['.docx', '.doc', '.rtf', '.odt']
        img_ext_list = ['.jpg', '.jpeg', '.bmp', '.tiff', '.tif', '.png']

        file_streams = list()
        # собираем список bytes_string
        for file in files_list:

            file_path = file[0]
            if not os.path.exists(file_path):
                continue
            file_rotate = file[1]
            file_ext = os.path.splitext(file_path)[1].lower()
            file_stream = io.BytesIO

            if file_ext in doc_ext_list:
                # если документ - сохраняем в pdf и читаем
                pdf_doc_file_path = get_temp_path(file_path) + '.pdf'
                # если сохранение успешно - читаем
                if DocConverter.convert_doc_to_pdf(file_path, pdf_doc_file_path):
                    file_stream = cls.get_stream_from_file(pdf_doc_file_path,
                                                           True)

            elif file_ext in img_ext_list:
                # если изображение - получаем поток через функцию
                file_stream = cls.get_pdf_streams_from_image(file_path,
                                                             file_rotate,
                                                             page_unit,
                                                             page_width,
                                                             page_height,
                                                             dpi,
                                                             True)

            elif file_ext == '.pdf':
                # если pdf, просто читаем
                file_stream = cls.get_stream_from_file(file_path, False)

            if file_stream is not None:
                file_streams.append(file_stream)

        return file_streams

    @classmethod
    def collect_pdf_from_streams(cls, file_streams, pdf_path):
        """
        Собираейт из потоков данных PDF
        :param file_streams: потоки данных, список
        :param pdf_path: путь для сохранения файла
        :return: True - если сохранилась, False, если нет
        """
        # создаем объект для записи файла
        pdf_file_writer = PdfFileWriter()
        # перебираем потоки
        for file_stream in file_streams:
            # читаем поток в pdf
            stream_pdf = PdfFileReader(file_stream)

            # перебираем страницы
            stream_page_num = stream_pdf.getNumPages()
            index = 0
            while index < stream_page_num:
                # добавляем страницу в новый объект
                pdf_file_writer.addPage(stream_pdf.getPage(index))
                index += 1

        try:
            output_stream = open(pdf_path, "wb")
            pdf_file_writer.write(output_stream)
            output_stream.close()
            return True
        except IOError:
            print('Файл ' + pdf_path + ' не сохранен!')
            error_text = sys.exc_info()[1]
            print(error_text)
            return False

    @classmethod
    def collect_pdf_from_file_list(cls, file_list, pdf_path):
        """
        Собирает и сохраняет pdf файл из входного списка

        :param file_list: [path, degree] путь к файлу и на сколько градусов его надо повернуть
        :param pdf_path: путь к сохраняемому файлу
        :return: True если сохранила и False если нет
        """
        file_streams = cls.get_pdf_bytes_from_files(file_list)
        if cls.collect_pdf_from_streams(file_streams, pdf_path):
            return True
        else:
            return False

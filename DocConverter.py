import os
import sys
import subprocess
import winreg
import time
import pathlib

import re
import comtypes.client


class DocConverter:
    """
    Конвертируем документы (*.doc, *.docx, *.odt, *.rtf)
    """

    @classmethod
    def get_app_from_registry(cls, app_exe_name):
        """Функция извлечения пути к приложению из реестра по имени
        исполняемого файла (например winword.exe)

        :param app_exe_name: имя исполняемого файла
        :return: список содержащий пару - [имя исполняемого файла, путь к файлу]
        None - если не найдены приложения
        """
        apps_root_key = 'SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\'
        error_text = 'Приложение ' + app_exe_name + ' не установлено!'
        try:
            app_key = apps_root_key + app_exe_name
            root_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE,
                                      app_key,
                                      0)
            [path, regtype] = winreg.QueryValueEx(root_key, None)
            winreg.CloseKey(root_key)

            if path != '' and os.path.exists(path):
                app_exec_path = path
                return app_exec_path

        except OSError:
            print(error_text)
            print(sys.exc_info()[1])
            return None

    @classmethod
    def convert_doc_to_pdf(cls, infile, outfile):
        """Функция преобразования документов в PDF, использует приоритетно Word
        если его нет - LibreOffice - если и его нет или не получилось преобразовать
         - возвращает False

        :param infile:
        :param outfile:
        :return: True если сконвертировал, False если нет
        """
        outfile = os.path.abspath(outfile)
        app_list = ['winword.exe', 'soffice.exe']
        is_file_converted = False
        if len(app_list) > 0:
            for app in app_list:

                if not is_file_converted:

                    if app == 'winword.exe':
                        is_file_converted = cls.convert_doc_to_pdf_word(infile,
                                                                        outfile)

                    elif app == 'soffice.exe':
                        is_file_converted = cls.convert_doc_to_pdf_libre(app,
                                                                         infile,
                                                                         outfile)

        else:
            print("При конвертации файла в PDF: "
                  + infile
                  + " произошла ошибка - не установлены Word и Libreoffice")

        return is_file_converted

    @classmethod
    def convert_doc_to_pdf_word(cls, infile, outfile):
        """Преобразует файл документ в PDF через COM вызов Word
        если вызвать не получилось - выдает ошибку

        :param infile: входной файл (.doc, .docx,.odt,.rtf)
        :param outfile: выходной файл (pdf)
        :return:
        """
        wd_format_pdf = 17
        is_file_saved = False

        try:
            word = comtypes.client.CreateObject('Word.Application')

        except OSError:
            print('Ошибка подключения к word, использую Libreoffice')
            is_file_saved = False
            return is_file_saved

        try:
            doc = word.Documents.Open(infile)

            if doc.SaveAs(outfile, FileFormat=wd_format_pdf) == 0:
                doc.Close()
                # пауза нужна - без нее Word не успеет сохранить
                time.sleep(0.5)
                is_file_saved = True

            else:
                doc.Close()
                is_file_saved = False

        except OSError:
            print('При конвертации файла ' + infile + ' произошла ошибка Word')
            print(OSError.args[1])
            is_file_saved = False

        finally:
            word.Quit()

        return is_file_saved

    @classmethod
    def convert_doc_to_pdf_libre(cls, app, infile, outfile):
        """Преобразует файл документ в PDF через вызов процесса
        LibreOffice

        :param app: - имя исполняемого файла (soffice.exe)
        :param infile: входной файл (.doc, .docx,.odt,.rtf)
        :param outfile: выходной файл (pdf)
        :return:
        """
        is_file_saved = False

        libre_exec = cls.get_app_from_registry(app)
        if libre_exec is None:
            return is_file_saved

        folder = os.path.split(infile)[0]
        args = [libre_exec,
                '--headless',
                '--convert-to',
                'pdf',
                '--outdir',
                folder,
                infile]

        try:
            process = subprocess.run(args,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)

            temp_filename = re.search('-> (.*?) using filter', process.stdout)
            os.rename(temp_filename, outfile)
            is_file_saved = True

        except OSError:
            print('При конвертации файла: '
                  + infile
                  + ' произошла ошибка LibreOffice:\n'
                  + process.stdout)
            print(OSError.args[1])

        return is_file_saved

    @classmethod
    def get_text_from_doc(cls, infile):
        """Функция извлечеия из документа текста, использует приоритетно Word
        если его нет - LibreOffice - если и его нет возвращает None

        :param infile:
        :return: True если сконвертировал, False если нет
        """
        exctracted_text = None
        app_list = ['winword.exe', 'soffice.exe']
        is_file_converted = False
        if len(app_list) > 0:
            for app in app_list:

                if not is_file_converted:

                    if app == 'winword.exe':
                        exctracted_text = cls.get_text_from_doc_word(infile)

                    elif app == 'soffice.exe':
                        exctracted_text = cls.get_text_from_doc_libre(app, infile)

        else:
            print("При конвертации файла в PDF: "
                  + infile
                  + " произошла ошибка - не установлены Word и Libreoffice")

        return exctracted_text

    @classmethod
    def get_text_from_doc_word(cls, infile):
        """Преобразует файл документ в PDF через COM вызов Word
        если вызвать не получилось - выдает ошибку

        :param infile: входной файл (.doc, .docx,.odt,.rtf)
        :param outfile: выходной файл (pdf)
        :return:
        """
        extracted_text = None
        try:
            word = comtypes.client.CreateObject('Word.Application')

        except OSError:
            print('Ошибка подключения к word, использую Libreoffice')
            return False

        try:
            doc = word.Documents.Open(infile)
            extracted_text = doc.Content.Text
            doc.Close()

        except OSError:
            print('При извлечении текста из файла: '
                  + infile
                  + ' произошла ошибка Word')
            print(OSError.args[1])

        finally:
            word.Quit()

        return extracted_text

    @classmethod
    def get_text_from_doc_libre(cls, app, infile):
        """Преобразует файл документ в PDF через вызов процесса
        LibreOffice

        :param app: - имя исполняемого файла (soffice.exe)
        :param infile: входной файл (.doc, .docx,.odt,.rtf)
        :param outfile: выходной файл (pdf)
        :return:
        """
        extracted_text = None

        libre_exec = cls.get_app_from_registry(app)
        if libre_exec is None:
            return extracted_text

        folder = os.path.split(infile)[0]
        args = [libre_exec,
                '--headless',
                '--convert-to',
                'txt',
                '--outdir',
                folder,
                infile]

        try:
            process = subprocess.run(args,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)

            temp_filename = re.search('-> (.*?) using filter', process.stdout)
            extracted_text = pathlib.Path(temp_filename).read_text()
            try:
                os.remove(temp_filename)
            except IOError:
                print('Не удалось удалить временный файл: '
                      + temp_filename)

        except OSError:
            print('При извлечении текста из файла: '
                  + infile
                  + ' произошла ошибка LibreOffice:\n'
                  + process.stdout)
            print(OSError.args[1])

        return extracted_text

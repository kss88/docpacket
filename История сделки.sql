

---Закрытые сессии по сделке
select crm_task_session.deal_id, person.last_name||' '||person.first_name||' '||person.middle_name as ФИО, 'АМИК' as deal_role ,
  crm_task_session.id as task_id, to_char(crm_task_session.created_at,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time, 'Начата сессия с клиентом' as action, 'Сессия' as task_type,'' as comment
from mastersber.crm_task_session
left join ms_person.person on person.cas_id = crm_task_session.cas_user_id
where crm_task_session.deal_id = ${dealid}
union
select crm_task_session.deal_id,person.last_name||' '||person.first_name||' '||person.middle_name as ФИО, 'АМИК' as deal_role ,
  crm_task_session.id as task_id, to_char(crm_task_session.closed_at ,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time, 'Завершена сессия с клиентом'as action,'Сессия' as task_type,'' as comment
from mastersber.crm_task_session
left join ms_person.person on person.cas_id = crm_task_session.cas_user_id
where crm_task_session.deal_id = $(dealid)
----
union
---История статусов
select deal_history.deal_id, null as ФИО, null as deal_role, deal_history.id as task_id, to_char(deal_history.created_time ,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time, new.comment as action, 'Статус' as task_type,'' as comment from ms_deal.deal_history
left join ms_deal.dictionary old on old.id = deal_history.old_status_id
left join ms_deal.dictionary new on new.id = deal_history.new_status_id
where deal_id = $(dealid)
---
union
---Закрытые задачи по сделке
select mastersber.crm_task_session.deal_id, person.last_name||' '||person.first_name||' '||person.middle_name as ФИО,
  'АМИК' as deal_role , tsk.id as task_id, to_char(tsk_hst.created_at ,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time,
  crm_task_type.name as action ,'Задача' as task_type, deal_note.body as comment from mastersber.crm_task_session
left join ms_person.person on person.cas_id = crm_task_session.cas_user_id
left join mastersber.crm_task tsk on tsk.deal_id = crm_task_session.deal_id
left join mastersber.crm_task_history tsk_hst on tsk_hst.task_id = tsk.id
left join mastersber.crm_task_type on crm_task_type.id = tsk.task_type_id
  left join mastersber.deal_note on tsk.id = deal_note.task_id

where crm_task_session.deal_id = $(dealid)
and (tsk_hst.old_status_id =1 and  tsk_hst.new_status_id =2 and tsk_hst.created_at >= crm_task_session.created_at and tsk_hst.created_at <= crm_task_session.closed_at)
---
union
---Звонки по сделке
select crm_activity.deal_id,  person.last_name||' '||person.first_name||' '||person.middle_name as ФИО, 'АМИК' as deal_role, call_id as task_id,
  to_char(call.time_stamp ,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time, crm_activity_type.name as action, 'Звонок' as task_type, call.duration::text as comment

from mastersber.crm_activity
  left join mastersber.crm_activity_type on crm_activity.type_id = mastersber.crm_activity_type.id
  left join obeliskdb.call on call.id = crm_activity.call_id
  left join mastersber."user" on mastersber."user".id = crm_activity.user_id
  left join ms_person.person on person.id = mastersber."user".person_id
  left join obeliskdb.business_case on obeliskdb.business_case.id = obeliskdb.call.business_case_id
where crm_activity.deal_id = $(dealid) and crm_activity.call_id is not null
---
union
---Заявки на ОДОН
select ms_deal.deal.id as deal_id,
  usr.second_name||' '||usr.first_name ||'('||partner_office.name||')' as МИК,
  case when approval.created_mik then 'МИК' else 'Партнер' end as deal_role,
  approval.id as task_id,
  to_char(approval.date ,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time, 'Заявка на ОДОН' as action,'Заявка на ОДОН' as task_type, approval.comment
from mastersber.approval
  left join ms_deal.deal on ms_deal.deal.transact_id = mastersber.approval.zid
  left join mastersber."user" usr on usr.cas_id = approval.user_id
  left join mastersber.partner_office on partner_office.id = approval.partner_office_id

where ms_deal.deal.id = $(dealid)
union
---Заметки
select note.deal_id ,person.last_name||' '||person.first_name||' '||person.middle_name as МИК , 'АМИК' as deal_role, note.id as task_id,
  to_char(note.created_at + interval '3 hours' ,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time,'Заметка' as action,'Заметка' as task_type,note.body as comment from mastersber.crm_task_session
left join ms_person.person on person.cas_id = crm_task_session.cas_user_id
left join mastersber.deal_note note on note.deal_id = crm_task_session.deal_id
where crm_task_session.deal_id = $(dealid)
and (note.created_at + interval '3 hours') >=crm_task_session.created_at and (note.created_at + interval '3 hours') <= crm_task_session.closed_at
union
---Чаты по сделке
(with members as (
select distinct ms_person.person.cas_id, person_id, confirmed_phone, ms_deal.dictionary.title, ms_person.person.last_name ||' '||ms_person.person.first_name ||' '||ms_person.person.middle_name as name from ms_deal.person
left join ms_person.person on ms_person.person.id = ms_deal.person.person_id
left join ms_deal.dictionary on ms_deal.person.person_type_id = dictionary.id
where ms_deal.person.deal_id = $(dealid) and ms_deal.person.person_type_id not in (21070,21080,21090)),

    members_phones as (
select  cas_id::text,person_id,members.confirmed_phone, string_agg(members.title, ',') AS deal_role, members.name from members
group by cas_id,person_id,confirmed_phone, name),
  users_cas as (select mv_chats."fromCasId", mv_chats."fromCasId"::int as cas_id from sbrf.mv_chats where room in (select transact_id from ms_deal.deal

where deal.id = $(dealid)))

select distinct   ms_deal.deal.id as deal_id,case
    when members_phones.name is null then usr.second_name ||' '||usr.first_name
    else members_phones.name
  end as name,
    case
    when members_phones.deal_role is null then 'АМИК'
    else members_phones.deal_role
  end as deal_role,
  0 as task_id,
  to_char(mv_chats.time ,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time,'Cообщение в чат' as action,'Чат' as task_type ,
  case when mv_chats.message is null then replace(substring(REGEXP_MATCHES(mv_chats.file,'filename"=>"'||'.+?"')::text,17),'\""}','') else mv_chats.message end as comment
  from sbrf.mv_chats
left join members_phones on members_phones.cas_id = mv_chats."fromCasId"
  left join users_cas on mv_chats."fromCasId" = users_cas."fromCasId"
  left join mastersber."user" usr on usr.cas_id = users_cas.cas_id
  left join ms_deal.deal on ms_deal.deal.transact_id = sbrf.mv_chats.room
where room in (select transact_id from ms_deal.deal

where deal.id = $(dealid)))
---
union
---Шапочка
select id as deal_id, null as ФИО, null as deal_role, null as task_id, to_char('2017-01-01'::timestamp,'YYYY.mm.dd hh24:mm:ss') as tsk_done_time,'История сделки №'||id||' ('||deal.transact_id||')' as action, 'start' as task_type, null  as comment from ms_deal.deal
where id = $(dealid)
order by tsk_done_time



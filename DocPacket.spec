# -*- mode: python -*-

import PyInstaller.utils.hooks
block_cipher = None

a = Analysis(['DocPacket.py'],
             pathex=['y:\\'],
             binaries=[],
             datas=[],
             hiddenimports=PyInstaller.utils.hooks.collect_submodules('pysnmp')+PyInstaller.utils.hooks.collect_submodules('patoolib')+['xlrd', 'sklearn', 'sklearn.neighbors.typedefs'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='DocPacket',
          debug=False,
          strip=False,
          upx=True,
		  console=True ,
		  icon='.\\icons\\icon.ico' )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='DocPacket')


# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 20:38:12 2017

@author: Sergey Kozlov
"""
import sys
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.externals import joblib
from sklearn.linear_model import SGDClassifier


class DocumentTypeDetector:
    """
    класс для получения типа документа по его имени / содержимому
    """
    vectorizer_filename: TfidfVectorizer
    model_filename: SGDClassifier
    is_filename_model_loaded = False
    vectorizer_text: TfidfVectorizer
    model_text: SGDClassifier
    is_text_model_loaded = False
    abbreviations_dict: dict
    is_abbreviations_dict_loaded = False

    def __init__(self,
                 should_load_filename_model=True,
                 should_load_text_model=False,
                 should_load_abbreviations_dict=True,
                 model_filename_path='./models/filename_sgdc.pkl',
                 vectorizer_filename_path='./models/filename_tfidf.pkl',
                 model_text_path='./models/text_sgdc.pkl',
                 vectorizer_text_path='./models/text_tfidf.pkl',
                 abbreviations_xl_path='./dicts/sokr.xlsx'):
        """
        функция загрузки моделей в класс
        :param should_load_filename_model: bool Загружать модель для имен файлов?
        :param should_load_text_model: bool Загружать модель для текста?
        :param model_filename_path: путь к модели дял имен файлов
        :param vectorizer_filename_path: путь к векторизатору для имен файлов
        :param model_text_path: путь к модели для текста
        :param vectorizer_text_path: путь к векторизатору текста
        :return:
        """
        """
        
        :return:
        """
        self.is_filename_model_loaded = False
        self.is_text_model_loaded = False
        self.is_abbreviations_dict_loaded = False
        self.model_filename = None
        self.vectorizer_filename = None
        self.model_text = None
        self.vectorizer_text = None
        self.abbreviations_dict = dict()

        if should_load_filename_model:
            [self.model_filename, self.vectorizer_filename] = \
                self.load_model_from_file(model_filename_path,
                                          vectorizer_filename_path)

        self.is_filename_model_loaded = (self.model_filename is not None
                                         and self.vectorizer_filename is not None)

        if should_load_text_model:
            [self.model_text, self.vectorizer_text] = \
                self.load_model_from_file(model_text_path,
                                          vectorizer_text_path)

        self.is_text_model_loaded = (self.model_text is not None
                                     and self.vectorizer_text is not None)

        if (should_load_filename_model == self.is_filename_model_loaded
            and should_load_text_model == self.is_text_model_loaded):

            if should_load_abbreviations_dict:
                self.abbreviations_dict = self.load_dict_from_excel_file(abbreviations_xl_path)
                self.is_abbreviations_dict_loaded = self.abbreviations_dict is not None

        else:
            print('При инициализации экземляра класса произошла ошибка!')

    @classmethod
    def load_dict_from_excel_file(cls, excel_file):
        """
        Загружает словарь из файла excel
        файл должен содержать 2 столбца short (первый столбец)
        и long (второй стобец) - расшифровка сокращения

        :param excel_file:
        :return: загруженный словарь
        """
        dict_for_load = None
        try:
            excel_frame = pd.read_excel(excel_file, index_col=0)
            dict_for_load = excel_frame.to_dict(orient='dict')['long']
        except IOError:
            print('Файл: ' + excel_file + ' не прочитан!')
            error_text = sys.exc_info()[1]
            print(error_text)

        except AttributeError:
            print('Файл: ' + excel_file + ' не соответсвует шаблону!')
            error_text = sys.exc_info()[1]
            print(error_text)

        return dict_for_load

    @classmethod
    def check_compatibility(cls, model, vectorizer):
        """
        Проверяет совместимость модель и векторизатора испытанием :-)
        :param model: модель
        :param vectorizer: векторизатор
        :return: True если совместимы False еслии нет
        """
        is_compatibility = False
        if type(vectorizer) is TfidfVectorizer and type(model) is SGDClassifier:

            try:
                test_text = 'паспорт паспорт'
                test_data = {'text': [test_text, test_text]}
                test_df = pd.DataFrame(data=test_data)
                test_matrix = vectorizer.transform(test_df['text'])
                model.predict(test_matrix)
                is_compatibility = True

            except Exception:
                print('Загруженные файлы не являются совместимыми!')
                error_text = sys.exc_info()[1]
                print(error_text)

        return is_compatibility

    def load_model_from_file(self, model_path, vectorizer_path):
        """
        Загружает модель и векторизатор из указанных путей в экземпляр класса
        загружает pkl файлы, созданные через joblib.
        Модель должна быть SGDClassifier
        Векторизер - TfidfVectorizer
        Модель должна быть обучена под векторизатор.
        :param model_path: path to model pkl
        :param vectorizer_path: path to vectorizer pkl
        :return: True if loaded, False is not
        """
        is_compatibility = False
        loaded_model = self.load_pkl(model_path)
        loaded_vectorizer = self.load_pkl(vectorizer_path)
        # чекаем, что все загрузилось
        is_loaded_model = loaded_model is not None
        is_loaded_vectorizer = loaded_vectorizer is not None
        # проверяем совместимость векторизера и модели испытанием
        if is_loaded_model and is_loaded_vectorizer:
            is_compatibility = self.check_compatibility(loaded_model,
                                                        loaded_vectorizer)
        result = [loaded_model, loaded_vectorizer]

        if is_compatibility is False:
            result = None

        return result

    @classmethod
    def load_pkl(cls, pkl_file_path):
        """
        Загружает через joblib файл в переменную
        проверка только на ошибки чтения.
        :param pkl_file_path: путь к файлу
        :return: Загруженная модель, None если не загружена
        """
        loaded_var = None

        try:
            loaded_var = joblib.load(pkl_file_path)

        except IOError:
            error_text = sys.exc_info()[1]
            print(error_text)

        return loaded_var

    def prepare_column(self,
                       df: pd.DataFrame,
                       column_name: str,
                       use_abbreviation=True):
        """
        Функция подготовки строкового столбца для обработки модели
        :param df: pandas  dfFrame содержащий столбец
        :param column_name: наименование столбца
        :param use_abbreviation: использовать словарь сокращений
        :return: dataSeries c обработанным столбцом
        """

        # Предобработка
        df[column_name] = df[column_name].astype(str)
        df[column_name] = df[column_name].str.lower()
        df[column_name] = df[column_name].str.replace(u'\n', u' ')
        df[column_name] = df[column_name].str.replace(u' - ?', u'-')
        df[column_name] = df[column_name].str.replace(u'.', ' ')
        df[column_name] = df[column_name].str.replace(u',', ' ')
        df[column_name] = df[column_name].str.replace(u'-', ' ')
        df[column_name] = df[column_name].str.replace(u'\s+', ' ')

        # обработка сокращений
        if use_abbreviation and self.is_abbreviations_dict_loaded:
            abbreviations = self.abbreviations_dict
            #
            for abbr in abbreviations.keys():
                df[column_name] = \
                    df[column_name].str.replace(u'%s' % abbr,
                                                u'%s' % (abbreviations[abbr]))

        df[column_name] = df[column_name].str.replace(u'[^а-яa-z -]', ' ')
        df[column_name] = df[column_name].str.replace('\d+', '')
        # удаляем лишние пробелы в конце и начале строки

        df[column_name] = df[column_name].fillna('')
        # подчищаем от двойных пробелов цифр и т.д.
        df[column_name] = df[column_name].str.replace(u'[a-z]', u' ')
        df[column_name] = df[column_name].str.replace(u'\s[а-я](\s|$)', u' ')
        df[column_name] = df[column_name].str.replace(u'\s+', ' ')
        df[column_name] = df[column_name].str.replace(u'^\s+$', '')
        df[column_name] = df[column_name].str.lstrip()
        return df[column_name]

    def prepare_str(self,
                    text: str,
                    use_abbreviation=True):
        """
        Функция подготовки строки/текста для модели
        :param text: текст
        :param use_abbreviation: использовать словарь сокращений
        :return: str c обработанным текстом
        """

        # Предобработка
        prepared_text = text.lower()
        prepared_text = prepared_text.replace(u'\n', u' ')
        prepared_text = prepared_text.replace(u' - ?', u'-')
        prepared_text = prepared_text.replace(u'.', ' ')
        prepared_text = prepared_text.replace(u',', ' ')
        prepared_text = prepared_text.replace(u'-', ' ')
        prepared_text = prepared_text.replace(u'\s+', ' ')

        # обработка сокращений
        if use_abbreviation and self.is_abbreviations_dict_loaded:
            abbreviations = self.abbreviations_dict
            #
            for abbr in abbreviations.keys():
                prepared_text = \
                    prepared_text.replace(u'%s' % abbr,
                                          u'%s' % (abbreviations[abbr]))

        prepared_text = prepared_text.replace(u'[^а-яa-z -]', ' ')
        prepared_text = prepared_text.replace('\d+', '')
        # подчищаем от двойных пробелов цифр и т.д.
        prepared_text = prepared_text.replace(u'[a-z]', u' ')
        prepared_text = prepared_text.replace(u'\s[а-я](\s|$)', u' ')
        prepared_text = prepared_text.replace(u'\s+', ' ')
        prepared_text = prepared_text.replace(u'^\s+$', '')
        prepared_text = prepared_text.lstrip()
        return prepared_text

    def prepare_df(self,
                   input_df,
                   file_column_name: str,
                   dir_column_name='foldername',
                   extension_column_name='ext',
                   use_abbreviation=True):
        """
        Функция группирует входной список файлов по названиям и выдает его
        с проставленной группой.
        На вход подаем DataFrame в котором есть столбецы-параметры. Они должен
        быть очищен от соли и расширения.
        :param input_df: pandas DataFrame содержащий столбцы с названиями в
        прочих параметрах.
        :param file_column_name: название столбца с названием файла (не путем!)
        обязательно очищен от солии без расширения
        :param dir_column_name: название столбца с названием папки в
        которой находится файл (не путь!) требования теже, что к предыдущему
        :param extension_column_name: название колонки с расширением
        :param use_abbreviation: использовать словарь аббревиатур
        :return: исходный DataFrame, с подготовленными столбцами или None,
        если не удалось подготовить
        """
        # копируем Data Frame в рабочую копию
        work_df = input_df.copy()
        try:
            # прогоняем через обработчик столбцы с именами файла и папки
            work_df[file_column_name] = self.prepare_column(work_df,
                                                            file_column_name,
                                                            use_abbreviation)
            work_df[dir_column_name] = self.prepare_column(work_df,
                                                           dir_column_name,
                                                           use_abbreviation)
            #  добавляем к названиям файлов расширения
            work_df[file_column_name] = (work_df[file_column_name]
                                         + ' '
                                         + work_df[extension_column_name])

        except Exception:
            print('Не удалось обработать данные')
            error_text = sys.exc_info()[1]
            print(error_text)
            work_df = None

        return work_df

    def predict(self,
                model,
                vectorizer,
                df,
                detect_col_name,
                cat_col_name,
                coefficient_col_name):
        """
        Запускает модель, если отработано с проблемами - выдает None


        :param model: модель
        :param vectorizer: векторизатор
        :param df: датафрейм со столбцами
        :param detect_col_name:  столбец по которому отработает модель
        :param cat_col_name: столбец в которую будет записан результат
        :param coefficient_col_name: столбец в который будет внесен коэффициент
        :return: df или None
        """
        try:
            tfidf_result = vectorizer.transform(df[detect_col_name])
            df[cat_col_name] = model.predict(tfidf_result)
            coefs_df = pd.DataFrame(model.decision_function(tfidf_result))
            df[coefficient_col_name] = coefs_df.max(axis=1)
            return df

        except Exception:
            error_text = sys.exc_info()[1]
            print(error_text)
            return None

    def predict_str(self,
                    model: SGDClassifier,
                    vectorizer: TfidfVectorizer,
                    text: str):
        """
        Запускает модель, если отработано с проблемами - выдает None


        :param model: модель
        :param vectorizer: векторизатор
        :param text: str текст
        :return: [категория, коэффициент] или None
        """
        try:
            tfidf_result = vectorizer.transform([text])
            category = self.model_filename.predict(tfidf_result)[0]
            coefs_df = pd.DataFrame(model.decision_function(tfidf_result))
            coefficient = coefs_df.max(axis=1)[0]
            return [category, coefficient]

        except Exception:
            error_text = sys.exc_info()[1]
            print(error_text)
            return None

    def detect_type_string(self,
                           text: str,
                           model_type='filename',
                           use_abbreviation=True):
        """
        Получаем категорию и коэффициент по строке текста
        модель должна быть загружена!
        :param text: str текст
        :param model_type: 'filename' или 'text'
        :param use_abbreviation: использовать словарь аббревиатур
        :return:[категория, коэффициент], None - если нештатная отработка.
        """
        is_model_loaded = False
        model = None
        vectorizer = None
        result = None

        if model_type == 'filename':
            is_model_loaded = self.is_filename_model_loaded
            model = self.model_filename
            vectorizer = self.vectorizer_filename

        elif model_type == 'text':
            is_model_loaded = self.is_text_model_loaded
            model = self.model_text
            vectorizer = self.vectorizer_text

        if is_model_loaded is False:
            print('Модель ' + model_type + ' не загружена!')
            return None

        prepared_text = self.prepare_str(text, use_abbreviation)
        prediction = self.predict_str(model, vectorizer, prepared_text)

        if prediction is not None:
            result = prediction

        return result

    def detect_type_filename(self,
                             df: pd.DataFrame,
                             filename_col_name: str,
                             dirname_col_name: str,
                             ext_col_name: str,
                             cat_col_name: str,
                             coefficient_col_name: str,
                             use_abbreviation=True):
        """
        Определяет по столбцам с именем файла и папки категорию документа.
        в случае сбоя на любом этапе - все сваливает в категорию unknow
        :param df: pandas DataFrame, содержащий колонки c названиями:
        :param filename_col_name: имя колонки с именем файла (без расширения
        и соли).
        :param dirname_col_name: имя колонки с именем папки с файлом
        требования те же, что и к имени файла
        :param ext_col_name: колонка с расширением (.jpg, например)
        :param cat_col_name: колонка в которую нужно зафиксировать
        категорию.
        :param coefficient_col_name: колонка в которую нужно зафиксировать
        коэффицциент, с которым была выбрана категория
        :param use_abbreviation: использовать словарь аббревиатур
        :return: df с заполненными столбцами из параметров, в категорию
        вносит категорию, если определил, если нет - unknow.
        """

        result_df = df.copy()
        # Проверяем загружена ли модель.  Если нет - выдаем сообщение и
        # проставляем категорию unknow в результат

        if self.is_filename_model_loaded is False:
            print('Модель для имен файлов не загружена,'
                  ' определение типа документа невозможно!')
            print('Помещаю все столбцы в категорию нераспознанных!')

        else:
            # подготавливаем данные
            result_df = self.prepare_df(result_df,
                                        filename_col_name,
                                        dirname_col_name,
                                        ext_col_name,
                                        use_abbreviation)
            # если данные подготовлены - запускаем модель
            if result_df is not None:
                result_df = self.predict(self.model_filename,
                                         self.vectorizer_filename,
                                         result_df,
                                         filename_col_name,
                                         cat_col_name,
                                         coefficient_col_name)
        # если неудачно отработала модель
        if result_df is None:
            result_df = df
            result_df[cat_col_name] = 'unknow'
            result_df[coefficient_col_name] = -100.
        return result_df

    def detect_type_text(self,
                         df,
                         text_col_name: str,
                         cat_col_name: str,
                         coefficient_col_name: str,
                         use_abbreviation=True):
        """
        Определяет по столбцам с именем файла и папки категорию документа.
        в случае сбоя на любом этапе - все сваливает в категорию unknow
        :param df: pandas DataFrame, содержащий колонки c названиями:
        :param text_col_name: имя колонки с текстом
        :param cat_col_name: колонка в которую нужно зафиксировать
        категорию.
        :param coefficient_col_name: колонка в которую нужно зафиксировать
        коэффицциент, с которым была выбрана категория
        :param use_abbreviation: использовать словарь сокращений
        :return: df с заполненными столбцами из параметров, в категорию
        вносит категорию, если определил, если нет - unknow.
        """

        result_df = df.copy()
        # Проверяем загружена ли модель.

        if self.is_text_model_loaded is False:
            print('Модель для имен файлов не загружена,'
                  ' определение типа документа невозможно!')
            print('Помещаю все столбцы в категорию нераспознанных!')

        else:
            # подготавливаем столбец
            result_df[text_col_name] = self.prepare_column(result_df,
                                                           text_col_name,
                                                           use_abbreviation)
            predicted_df = self.predict(self.model_text,
                                        self.vectorizer_text,
                                        result_df,
                                        text_col_name,
                                        cat_col_name,
                                        coefficient_col_name)
            # если удачно отработала модель
            if predicted_df is not None:
                result_df[cat_col_name] = predicted_df[cat_col_name]
                result_df[coefficient_col_name] = predicted_df[coefficient_col_name]
                result_df[cat_col_name] = result_df[cat_col_name].fillna('unknow')

            else:

                result_df[cat_col_name] = 'unknow'
                result_df[coefficient_col_name] = -100.
        return result_df


# блок работы с моделями недописан :-) модели подогнаны руками
# тут по идее нужны блоки для обучения модели.
# пропи
def fit_model(self, column, res_col):
    """
    Подгоняет модель имени файла по размеченным данным.
    :param self:
    :param column:
    :param res_col:
    :return:
    """
    data_ = pd.DataFrame()
    data_ = pd.read_excel('./xls/result_for_filename.xlsx')
    data = self.prepare_df(data_)

    tfidf = TfidfVectorizer(min_df=1, ngram_range=(1, 5))
    tfidf.fit(data[column])
    joblib.dump(tfidf, './models/filename_tfidf.pkl')

    x_train_tfidf = tfidf.transform(data[column])

    clf = SGDClassifier(loss='hinge',
                        penalty='l2',
                        alpha=1e-3,
                        max_iter=5,
                        random_state=42)
    clf.fit(x_train_tfidf, data[res_col])
    joblib.dump(clf, './models/filename_sgdc.pkl')
